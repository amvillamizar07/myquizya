import 'package:flutter/material.dart';
import 'package:myquizya/src/services/pregunstas_service.dart';
import 'package:myquizya/src/widgets/pregunta_item_widget.dart';
import 'package:provider/provider.dart';

class DataPreguntasSearch extends SearchDelegate {
  String seleccion = '';

  late PreguntasService preguntasService;

  @override
  List<Widget> buildActions(BuildContext context) {
    preguntasService = Provider.of<PreguntasService>(context);

    // Las acciones de nuestro appbar
    return <Widget>[
      IconButton(
        onPressed: () {
          query = '';
        },
        icon: Icon(Icons.clear),
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Icono a la izquierda
    return IconButton(
      onPressed: () {
        close(context, null);
      },
      icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Crear los resultados que vamos a mostrar
    return Center(
      child: Container(
        width: 100.0,
        height: 100.0,
        color: Colors.redAccent,
        child: Text(seleccion),
      ),
    );
    // throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Sugerencias que aparece cuand la persiona escribe
    if (query.isEmpty) {
      return Container();
    }
    preguntasService.buscarPeliculas(query);

    return ListView.builder(
      itemCount: preguntasService.resultadosSearch.length,
      itemBuilder: (BuildContext context, int index) {
        // preguntasService.resultadosSearch[index].uniqueId = '';
        return PreguntaItem(
          index: index,
          preguntas: preguntasService.resultadosSearch,
          search: true,
        );
      },
    );
  }
}
