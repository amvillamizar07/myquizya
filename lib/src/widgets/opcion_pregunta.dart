import 'package:flutter/material.dart';
import 'package:myquizya/src/services/pregunstas_service.dart';

class OpcionPregunta extends StatefulWidget {
  OpcionPregunta(
      {Key? key,
      required this.pregunta,
      required this.opcion,
      this.i,
      required this.view})
      : super(key: key);

  late Pregunta pregunta;
  late Opcione opcion;
  late bool view;
  int? i;

  @override
  State<OpcionPregunta> createState() => _OpcionPreguntaState();
}

class _OpcionPreguntaState extends State<OpcionPregunta> {
  @override
  Widget build(BuildContext context) {
    Widget response = Container();
    switch (widget.pregunta.typePregunta) {
      case 'unica_opcion':
        response = ListTile(
          leading: Radio<String>(
            value: widget.opcion.key.toString(),
            groupValue: widget.pregunta.paramsOpciones.trueOpcion,
            onChanged: widget.view
                ? null
                : (value) {
                    setState(() {
                      widget.pregunta.paramsOpciones.trueOpcion = value;
                    });
                  },
          ),
          title: TextFormField(
            initialValue: widget.opcion.label,
            enabled: !widget.view,
            onChanged: (value) {
              widget.opcion.label = value;
            },
          ),
        );
        break;
      case 'bool':
        response = ListTile(
          leading: Radio<String>(
            value: widget.opcion.key.toString(),
            groupValue: widget.pregunta.paramsOpciones.trueOpcion,
            onChanged: widget.view
                ? null
                : (value) {
                    setState(() {
                      widget.pregunta.paramsOpciones.trueOpcion = value;
                    });
                  },
          ),
          title: TextFormField(
            initialValue: widget.opcion.label,
            enabled: !widget.view,
            onChanged: (value) {
              widget.opcion.label = value;
            },
          ),
        );
        break;
      case 'multiple_respuesta':
        response = ListTile(
          leading: Checkbox(
            value: widget.opcion.isTrue,
            onChanged: widget.view
                ? null
                : (value) {
                    setState(() {
                      widget.opcion.isTrue = value!;
                    });
                  },
          ),
          title: TextFormField(
            initialValue: widget.opcion.label,
            enabled: !widget.view,
            onChanged: (value) {
              widget.opcion.label = value;
            },
          ),
        );
        break;
    }
    return response;
  }
}
