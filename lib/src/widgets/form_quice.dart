import 'package:flutter/material.dart';
import 'package:myquizya/src/models/quices_model.dart';
import 'package:myquizya/src/services/pregunstas_service.dart';
import 'package:myquizya/src/services/quices_service.dart';
import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/utils/utils.dart';
import 'package:myquizya/src/widgets/opcion_pregunta.dart';
import 'package:provider/provider.dart';

class FormQuice extends StatefulWidget {
  FormQuice({Key? key, required this.quice, this.view = false})
      : super(key: key);

  final bool view;
  final Quice quice;

  @override
  State<FormQuice> createState() => _FormQuiceState();
}

class _FormQuiceState extends State<FormQuice> {
  final formKey = GlobalKey<FormState>();

  late QuicesService quicesService;
  late PreguntasService preguntasService;

  @override
  Widget build(BuildContext context) {
    quicesService = Provider.of<QuicesService>(context, listen: false);
    preguntasService = Provider.of<PreguntasService>(context);
    final scollBarController = ScrollController(initialScrollOffset: 0);

    return Container(
      child: Scrollbar(
        thickness: 10,
        isAlwaysShown: true,
        controller: scollBarController,
        child: ListView(
          controller: scollBarController,
          shrinkWrap: true,
          padding:
              EdgeInsets.only(right: 20.0, top: 8.0, left: 16.0, bottom: 24.0),
          children: <Widget>[
            _nombreQuice(),
            Divider(height: 10.0),
            _selectMateria(),
            Divider(height: 10.0),
            _tema(),
            Divider(height: 10.0),
            _listaPreguntasSelected(),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: <Widget>[
            //     _botonAgregar(),
            //     _otraOpcion(),
            //   ],
            // ),
            // Divider(
            //   height: 15.0,
            // ),
            // view ? Container() : _btnSubmit(),
          ],
        ),
      ),
    );
  }

  Widget _nombreQuice() {
    return TextFormField(
      initialValue: widget.quice.nombre,
      enabled: !widget.view,
      decoration: InputDecoration(labelText: 'Nombre quiz'),
      onChanged: (value) => widget.quice.nombre = value.toString(),
    );
  }

  Widget _tema() {
    return TextFormField(
      initialValue: widget.quice.tema,
      enabled: !widget.view,
      decoration: InputDecoration(labelText: 'Tema'),
      onChanged: (value) => widget.quice.tema = value.toString(),
    );
  }

  Widget _selectMateria() {
    if (widget.view) {
      final _listaMaterias = listaMaterias();
      return TextFormField(
        initialValue: _listaMaterias[widget.quice.materia]!['text'].toString(),
        enabled: false,
        decoration: InputDecoration(labelText: 'Materia'),
      );
    } else {
      return DropdownButton<String>(
        isExpanded: true,
        itemHeight: 55.0,
        value: widget.quice.materia,
        icon: const Icon(Icons.arrow_drop_down_rounded),
        iconSize: 18,
        underline: Container(
          height: 2,
          color: miTema.primaryColor,
        ),
        onChanged: widget.view
            ? null
            : (String? newValue) {
                setState(() {
                  widget.quice.materia = newValue!;
                });
              },
        hint: Center(child: Text('Materia')),
        items: _generarListaItemTipo(),
      );
    }
  }

  /// Retorna la lista de items para el tipod e pregunta
  List<DropdownMenuItem<String>> _generarListaItemTipo() {
    List<DropdownMenuItem<String>> respuesta = [];
    listaMaterias().forEach((key, value) {
      respuesta.add(DropdownMenuItem<String>(
        value: key,
        child: Text(value['text']),
      ));
    });
    return respuesta.toList();
  }

  Widget _listaPreguntasSelected() {
    List<Pregunta> _listaPreguntas = quicesService.preguntasSelected;

    return ListView.builder(
      shrinkWrap: true,
      itemCount: _listaPreguntas.length,
      itemBuilder: (BuildContext context, int index) {
        List<Widget> listOpciones = _listaPreguntas[index]
            .paramsOpciones
            .opciones
            .map((e) => OpcionPregunta(
                opcion: e, view: true, pregunta: _listaPreguntas[index]))
            .toList();
        return Container(
          key: UniqueKey(),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        child: Text(
                          '${index + 1}. ${_listaPreguntas[index].enunciado}',
                          style: TextStyle(fontSize: 18.0),
                        )),
                    Column(children: listOpciones),
                    SizedBox(height: 16.0),
                    Row(
                      children: [
                        _infoPuntos(_listaPreguntas, index),
                        _infoTiempo(_listaPreguntas, index),
                      ],
                    )
                  ],
                ),
              ),
              widget.view
                  ? Container()
                  : IconButton(
                      onPressed: () {
                        setState(() {
                          quicesService.preguntasSelected
                              .removeWhere((element) {
                            return _listaPreguntas[index].id == element.id;
                          });
                        });
                      },
                      icon: Icon(Icons.remove),
                    )
            ],
          ),
        );
      },
    );
  }

  Widget _infoPuntos(List<Pregunta> _listaPreguntas, int index) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Puntos:'),
        Container(
          // width: MediaQuery.of(context).size.width / 2,
          width: 100.0,
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          // alignment: Alignment.center,
          child: widget.view
              ? Text(
                  _listaPreguntas[index].puntos != null
                      ? _listaPreguntas[index].puntos.toString()
                      : '',
                  style: TextStyle(fontSize: 20.0))
              : TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    isDense: true,
                  ),
                  onChanged: (String value) {
                    _listaPreguntas[index].puntos =
                        value != '' ? int.parse(value) : null;
                  },
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  initialValue: _listaPreguntas[index].puntos != null
                      ? _listaPreguntas[index].puntos.toString()
                      : '',
                  enabled: true,
                ),
        ),
      ],
    );
  }

  Widget _infoTiempo(List<Pregunta> _listaPreguntas, int index) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Tiempo:'),
        Container(
          // width: MediaQuery.of(context).size.width / 2,
          width: 100.0,
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          // alignment: Alignment.center,
          child: widget.view
              ? Text(
                  _listaPreguntas[index].segundos != null
                      ? '${_listaPreguntas[index].segundos.toString()} s.'
                      : '',
                  style: TextStyle(fontSize: 20.0))
              : TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    isDense: true,
                  ),
                  onChanged: (String value) {
                    _listaPreguntas[index].segundos =
                        value != '' ? int.parse(value) : null;
                  },
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  initialValue: _listaPreguntas[index].segundos != null
                      ? _listaPreguntas[index].segundos.toString()
                      : '',
                  enabled: true,
                ),
        ),
      ],
    );
  }
}
