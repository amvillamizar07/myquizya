import 'package:flutter/material.dart';

/// Dibuja el wedget que tiene como comte el popupmenuitem
class ItemButtonOpcion extends StatelessWidget {
  const ItemButtonOpcion({
    Key? key,
    required this.text,
    required this.icon,
  }) : super(key: key);

  final String text;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 10),
          child: Icon(icon),
        ),
        Text(text)
      ],
    );
  }
}
