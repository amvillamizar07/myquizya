import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  const Logo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final gradiente = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(color: Color.fromRGBO(255, 255, 255, 1)
          // gradient: LinearGradient(
          //   begin: FractionalOffset(0.0, 0.6),
          //   end: FractionalOffset(0.0, 1),
          //   colors: [
          //     Color.fromRGBO(52, 54, 101, 1.0),
          //     Color.fromRGBO(35, 37, 57, 1.0),
          //   ],
          // ),
          ),
    );

    final logoUni = Image(
      image: AssetImage('assets/img/logo-udi-web.png'),
      width: 300.0,
    );
    // final cajaRosa = Transform.rotate(
    //   angle: -pi / 5.0,
    //   child: Container(
    //     height: 300.0,
    //     width: 300.0,
    //     decoration: BoxDecoration(
    //       borderRadius: BorderRadius.circular(80.0),
    //       gradient: LinearGradient(
    //         colors: [
    //           Color.fromRGBO(240, 163, 0, 1.0),
    //           Color.fromRGBO(241, 142, 172, 1.0),
    //         ],
    //       ),
    //     ),
    //   ),
    // );

    return Stack(
      children: <Widget>[
        gradiente,
        Padding(
          padding: const EdgeInsets.only(left: 16.0),
          child: logoUni,
        ),
      ],
    );
  }
}
