import 'package:flutter/material.dart';
import 'package:myquizya/src/models/preguntas_model.dart';
import 'package:myquizya/src/services/pregunstas_service.dart';
import 'package:myquizya/src/services/quices_service.dart';
import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/utils/utils.dart';
import 'package:provider/provider.dart';

class FormPregunta extends StatefulWidget {
  FormPregunta(
      {Key? key, required this.pregunta, this.view = false, this.main = true})
      : super(key: key);

  final bool view;
  final bool main;
  final Pregunta pregunta;

  @override
  State<FormPregunta> createState() =>
      _FormPreguntaState(pregunta: pregunta, view: view);
}

class _FormPreguntaState extends State<FormPregunta> {
  final formKey = GlobalKey<FormState>();
  Pregunta pregunta;
  bool view;

  _FormPreguntaState({required this.pregunta, required this.view});

  @override
  Widget build(BuildContext context) {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _tipoPregunta(),
        _enunciado(),
        _listaOpcionesBuilder(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _botonAgregar(),
            _otraOpcion(),
          ],
        ),
        Divider(
          height: 15.0,
        ),
        view ? Container() : _btnSubmit(context),
      ],
    );
  }

  Widget _tipoPregunta() {
    return DropdownButton<String>(
      isExpanded: true,
      value: pregunta.typePregunta,
      icon: const Icon(Icons.arrow_drop_down_rounded),
      iconSize: 18,
      elevation: 24,
      // style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: miTema.primaryColor,
      ),
      onChanged: view
          ? null
          : (String? newValue) {
              setState(() {
                pregunta.typePregunta = newValue!;
                if (pregunta.typePregunta == 'bool') {
                  pregunta.paramsOpciones.opciones = [
                    Opcione(
                        key: UniqueKey().toString(),
                        label: 'Verdarero',
                        isTrue: true),
                    Opcione(
                        key: UniqueKey().toString(),
                        label: 'Falso',
                        isTrue: true),
                  ];
                }
              });
            },
      hint: Center(child: Text('Selecione tipo pregunta')),
      items: _generarListaItemTipo(),
    );
  }

  /// Retorna la lista de items para el tipod e pregunta
  List<DropdownMenuItem<String>> _generarListaItemTipo() {
    List<DropdownMenuItem<String>> respuesta = [];
    listaTiposPreguntas().forEach((key, value) {
      respuesta.add(DropdownMenuItem<String>(
        value: key,
        child: Text(value['text']),
      ));
    });
    return respuesta.toList();
  }

  Widget _enunciado() {
    return TextFormField(
      initialValue: pregunta.enunciado,
      maxLines: 5,
      minLines: 2,
      enabled: !view,
      decoration: InputDecoration(labelText: 'Pregunta'),
      onChanged: (value) => pregunta.enunciado = value.toString(),
    );
  }

  Widget _botonAgregar() {
    final ButtonStyle style = ElevatedButton.styleFrom(
      elevation: 3.0,
      textStyle: const TextStyle(fontSize: 20),
    );
    return view
        ? Container()
        : ElevatedButton(
            onPressed: () {
              setState(() {
                print('click');
                pregunta.paramsOpciones.opciones.add(Opcione(
                    label: '', isTrue: false, key: UniqueKey().toString()));
              });
            },
            style: style,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
              child: Text('Añadir opción'),
            ),
          );
  }

  Widget _listaOpcionesBuilder() {
    List<Opcione> opciones = pregunta.paramsOpciones.opciones;

    return Container(
      constraints: BoxConstraints(
        maxHeight: MediaQuery.of(context).size.height * 0.4,
      ),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: opciones.length,
        itemBuilder: (BuildContext context, int i) {
          final opcion = opciones[i];

          if (view) {
            return _formListaOpciones(opcion, i);
          } else {
            return Dismissible(
              onDismissed: (_) {
                opciones.removeAt(i);
              },
              key: UniqueKey(),
              child: _formListaOpciones(opcion, i),
              background: Container(
                color: Colors.red,
              ),
            );
          }
        },
      ),
    );
  }

  Widget _formListaOpciones(Opcione opcion, int i) {
    Widget response = Container();
    switch (pregunta.typePregunta) {
      case 'unica_opcion':
        response = ListTile(
          leading: Radio<String>(
            value: opcion.key.toString(),
            groupValue: pregunta.paramsOpciones.trueOpcion,
            onChanged: view
                ? null
                : (value) {
                    setState(() {
                      pregunta.paramsOpciones.trueOpcion = value;
                    });
                  },
          ),
          title: TextFormField(
            initialValue: opcion.label,
            enabled: !view,
            onChanged: (value) {
              opcion.label = value;
            },
          ),
        );
        break;
      case 'bool':
        response = ListTile(
          leading: Radio<String>(
            value: opcion.key.toString(),
            groupValue: pregunta.paramsOpciones.trueOpcion,
            onChanged: view
                ? null
                : (value) {
                    setState(() {
                      pregunta.paramsOpciones.trueOpcion = value;
                    });
                  },
          ),
          title: TextFormField(
            initialValue: opcion.label,
            enabled: !view,
            onChanged: (value) {
              opcion.label = value;
            },
          ),
        );
        break;
      case 'multiple_respuesta':
        response = ListTile(
          leading: Checkbox(
            value: opcion.isTrue,
            onChanged: view
                ? null
                : (value) {
                    setState(() {
                      opcion.isTrue = value!;
                    });
                  },
          ),
          title: TextFormField(
            initialValue: opcion.label,
            enabled: !view,
            onChanged: (value) {
              opcion.label = value;
            },
          ),
        );
        break;
    }
    return response;
  }

  Widget _otraOpcion() {
    return Row(
      children: [
        Text('Otra opción'),
        Switch(
          value: pregunta.paramsOpciones.otraOpcion,
          // activeColor: Colors.deepPurple,
          onChanged: view
              ? null
              : (value) => setState(() {
                    pregunta.paramsOpciones.otraOpcion = value;
                  }),
        ),
      ],
    );
  }

  Widget _btnSubmit(BuildContext context) {
    List<Pregunta> listPreguntas =
        Provider.of<PreguntasService>(context).preguntas;
    QuicesService quicesService = Provider.of<QuicesService>(context);

    final ButtonStyle style = ElevatedButton.styleFrom(
      elevation: 3.0,
      textStyle: const TextStyle(fontSize: 20),
      padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            style: style,
            onPressed: () {
              if (widget.main && pregunta.id != null) {
                int indexEditado = listPreguntas
                    .lastIndexWhere((element) => element.id == pregunta.id);
                listPreguntas[indexEditado] = pregunta;
              } else {
                pregunta.id = UniqueKey().toString();
                listPreguntas.add(pregunta);
                if (!widget.main) {
                  quicesService.addPreguntasSelected = pregunta;
                }
              }
              setState(() {
                mostrarAlerta(
                    context: context,
                    title: 'Listo',
                    mensaje: 'Pregunta guarda');
              });
            },
            child: Row(
              children: [
                Icon(Icons.save),
                Text('Guardar'),
              ],
            )),
      ],
    );
  }
}
