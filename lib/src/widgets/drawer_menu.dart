import 'package:flutter/material.dart';
import 'package:myquizya/src/page/login_page.dart';
import 'package:myquizya/src/page/perfil_page.dart';
import 'package:myquizya/src/page/preguntas/preguntas_page.dart';
import 'package:myquizya/src/theme/thema.dart';

class DrawerMenu extends StatelessWidget {
  const DrawerMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              color: miTema.colorScheme.primary,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Andres Villamizar',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                  ),
                ),
                Image(image: AssetImage('assets/img/logo-udi-web.png'))
              ],
            ),
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text('Perfil'),
            onTap: () => Navigator.pushNamed(context, PerfilPage.routeName),
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            onTap: () {
              Navigator.restorablePushNamedAndRemoveUntil(
                  context, LoginPage.routeName, (route) => false);
            },
            title: Text('Cerrar sesión'),
          ),
          // ListTile(
          //   leading: Icon(Icons.history),
          //   title: Text('Historial de quices'),
          // ),
        ],
      ),
    );
    ;
  }
}
