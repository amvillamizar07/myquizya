import 'package:flutter/material.dart';
import 'package:myquizya/src/services/usuario_service.dart';

import 'dart:async';
import 'package:provider/provider.dart';

import 'package:myquizya/src/utils/utils.dart';
import 'package:myquizya/src/services/presentacion_quiz_service.dart';
import 'package:myquizya/src/services/quices_service.dart';
import 'package:myquizya/src/widgets/logo_wiget.dart';

class PreguntaPresentacion extends StatefulWidget {
  PreguntaPresentacion({Key? key, required this.pregunta, required this.i})
      : super(key: key);

  int i;
  Pregunta pregunta;

  @override
  _PreguntaPresentacionState createState() => _PreguntaPresentacionState();
}

class _PreguntaPresentacionState extends State<PreguntaPresentacion> {
  bool init = true;
  bool respondido = false;
  late int segundos = widget.pregunta.segundos ?? 50;
  late Timer timer;
  // String selected = '';
  // bool checked = false;
  late NavigationPresentacionQuiz navigationPresentacionQuiz;
  late UsuarioService usuarioService;
  late String tipoUsuario;

  @override
  Widget build(BuildContext context) {
    navigationPresentacionQuiz =
        Provider.of<NavigationPresentacionQuiz>(context);
    usuarioService = Provider.of<UsuarioService>(context);

    tipoUsuario = usuarioService.tipoUsuario;

    if (init) {
      init = false;
      iniciarCronometro();
    }

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          TextButton(
              onPressed: null,
              child: Text(
                'Puntos: ${widget.pregunta.puntos}',
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
      body: SafeArea(
        child: Stack(
          children: [
            Logo(),
            SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 14.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 120.0),
                  respondido ? _mensajeEspera() : Container(),
                  _displayHora(),
                  respondido ? Container() : _displayEnunciado(),
                  respondido ? Container() : _displayOptciones(),
                  Divider(height: 20.0),
                  respondido || tipoUsuario == '1'
                      ? Container()
                      : _btnGuardar(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void iniciarCronometro() {
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      this.segundos -= 1;
      if (mounted) {
        setState(() {});
        if (this.segundos <= 0) {
          timer.cancel();
          navigationPresentacionQuiz.paginaActual = widget.i + 1;
        }
      }
    });
  }

  Widget _displayHora() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _displayNumber(formaterTiempo('minuto', this.segundos), 'Minutos'),
          SizedBox(width: 15.0),
          _displayNumber(formaterTiempo('segundos', this.segundos), 'Segundos'),
        ],
      ),
    );
  }

  Widget _displayNumber(String value, String param) {
    return Column(
      children: [
        SizedBox.fromSize(
          size: Size(60, 60),
          child: ClipOval(
            child: Material(
              color: this.segundos <= 10
                  ? Color.fromRGBO(255, 9, 9, 1.0)
                  : Color.fromRGBO(6, 195, 0, 1.0),
              child: InkWell(
                splashColor: Colors.blueAccent,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(value,
                        style: TextStyle(color: Colors.white, fontSize: 30.0))
                  ],
                ),
              ),
            ),
          ),
        ),
        Text(param),
      ],
    );
  }

  _displayEnunciado() {
    return TextFormField(
        initialValue: '${widget.pregunta.enunciado}',
        minLines: 1,
        enabled: false);
  }

  _displayOptciones() {
    final opciones = widget.pregunta.paramsOpciones.opciones;
    List<Widget> listOpciones = opciones.map((opcion) {
      Widget response = Container();
      switch (widget.pregunta.typePregunta) {
        case 'unica_opcion':
          response = ListTile(
            leading: Radio<String>(
              value: opcion.key.toString(),
              groupValue: widget.pregunta.paramsOpciones.radioChecked,
              onChanged: tipoUsuario == '1'
                  ? null
                  : (value) {
                      setState(() {
                        widget.pregunta.paramsOpciones.radioChecked = value!;
                      });
                    },
            ),
            title: TextFormField(
              initialValue: opcion.label,
              enabled: false,
            ),
          );
          break;
        case 'bool':
          response = ListTile(
            leading: Radio<String>(
              value: opcion.key.toString(),
              groupValue: widget.pregunta.paramsOpciones.radioChecked,
              onChanged: tipoUsuario == '1'
                  ? null
                  : (value) {
                      setState(() {
                        widget.pregunta.paramsOpciones.radioChecked = value!;
                      });
                    },
            ),
            title: TextFormField(
              initialValue: opcion.label,
              enabled: false,
            ),
          );
          break;
        case 'multiple_respuesta':
          response = ListTile(
            leading: Checkbox(
              value: opcion.checked == null ? false : opcion.checked,
              onChanged: tipoUsuario == '1'
                  ? null
                  : (value) {
                      setState(() {
                        opcion.checked = value == null ? false : value;
                      });
                    },
            ),
            title: TextFormField(
              initialValue: opcion.label,
              enabled: false,
            ),
          );
          break;
      }
      return response;
    }).toList();

    return Column(children: listOpciones);
  }

  Widget _btnGuardar() {
    return SizedBox.fromSize(
      size: Size(60, 60),
      child: ClipOval(
        child: Material(
          color: Color.fromRGBO(6, 195, 0, 1.0),
          child: InkWell(
            onTap: () {
              respondido = true;
              setState(() {});
            },
            splashColor: Colors.blueAccent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.check,
                  color: Colors.white,
                  size: 45.0,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _mensajeEspera() {
    return Column(
      children: [
        Icon(
          Icons.lock_clock,
          size: 72.0,
        ),
        Divider(height: 10.0),
        Text(
          'Tiempo para la siguiente pregunta',
          style: TextStyle(fontSize: 20.0),
        ),
        Divider(height: 10.0),
      ],
    );
  }
}
