import 'dart:async';

import 'package:flutter/material.dart';
import 'package:myquizya/src/page/home_alumno_page.dart';
import 'package:myquizya/src/page/tabs_page.dart';
import 'package:myquizya/src/services/usuario_service.dart';
import 'package:myquizya/src/theme/thema.dart';

import 'package:provider/provider.dart';

import 'package:myquizya/src/services/presentacion_quiz_service.dart';
import 'package:myquizya/src/widgets/logo_wiget.dart';

class ResultadosIntermedios extends StatefulWidget {
  ResultadosIntermedios({Key? key, required this.i, required this.respuesta})
      : super(key: key);

  int i;
  bool respuesta;

  @override
  _ResultadosIntermediosState createState() => _ResultadosIntermediosState();
}

class _ResultadosIntermediosState extends State<ResultadosIntermedios> {
  bool init = true;
  late NavigationPresentacionQuiz navigationPresentacionQuiz;
  late UsuarioService usuarioService;
  late String tipoUsuario;

  @override
  Widget build(BuildContext context) {
    navigationPresentacionQuiz =
        Provider.of<NavigationPresentacionQuiz>(context);
    usuarioService = Provider.of<UsuarioService>(context);
    tipoUsuario = usuarioService.tipoUsuario;

    if (init) {
      init = false;
      if (navigationPresentacionQuiz.paginaActual <
              navigationPresentacionQuiz.totalPages &&
          tipoUsuario != '1') {
        Timer(Duration(seconds: 15), () {
          navigationPresentacionQuiz.paginaActual = widget.i + 1;
        });
      }
    }

    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Logo(),
            SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 100.0),
                  tipoUsuario == '1' ? Container() : _resultadoRespuesta(),
                  SizedBox(height: 30.0),
                  _titleSession(),
                  _listaParticipantes(),
                  tipoUsuario == '1' &&
                          navigationPresentacionQuiz.totalPages !=
                              navigationPresentacionQuiz.paginaActual
                      ? _botonNext()
                      : Container(),
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: navigationPresentacionQuiz.totalPages ==
              navigationPresentacionQuiz.paginaActual
          ? FloatingActionButton(
              onPressed: () {
                switch (usuarioService.tipoUsuario) {
                  case "1":
                    Navigator.restorablePushNamedAndRemoveUntil(
                        context, TabsPage.routeName, (route) => false,
                        arguments: {'tabIndex': 1});
                    break;

                  default:
                    Navigator.pushReplacementNamed(
                        context, HomeAlumnoPage.routeName);
                }
              },
              child: Icon(Icons.check_sharp))
          : null,
    );
  }

  Widget _listaParticipantes() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: 20.0),
      child: DataTable(
        columns: [
          DataColumn(label: Icon(Icons.lightbulb_outline_sharp)),
          DataColumn(label: Text('Alumnos')),
          DataColumn(label: Text('Puntos')),
        ],
        rows: [
          DataRow(
            cells: <DataCell>[
              DataCell(Text('1')),
              DataCell(Text('Juan vargas')),
              DataCell(Text('100')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('2')),
              DataCell(Text('Andres Villamizar Villamizar')),
              DataCell(Text('90')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('3')),
              DataCell(Text('Laudy Rondon Ardila')),
              DataCell(Text('75')),
            ],
          ),
          DataRow(
            cells: <DataCell>[
              DataCell(Text('4')),
              DataCell(Text('Peptito Perez')),
              DataCell(Text('65')),
            ],
          ),
        ],
      ),
    );
  }

  Widget _titleSession() {
    return Container(
      child: Text(
        'Puntuación',
        style: TextStyle(fontSize: 32.0, color: miTema.primaryColor),
      ),
    );
  }

  Widget _resultadoRespuesta() {
    return SizedBox.fromSize(
      size: Size(100, 100),
      child: ClipOval(
        child: Material(
          color: widget.respuesta
              ? Colors.greenAccent[400]
              : Colors.redAccent[400],
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  widget.respuesta ? Icons.check : Icons.close,
                  color: Colors.white,
                  size: 80.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _botonNext() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: SizedBox.fromSize(
        size: Size(200, 50),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(4.0),
          child: Material(
            color: Color.fromRGBO(6, 195, 0, 1.0),
            child: InkWell(
              onTap: () {
                navigationPresentacionQuiz.paginaActual = widget.i + 1;
              },
              splashColor: Colors.blueAccent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.skip_next, color: Colors.white),
                  SizedBox(width: 15.0),
                  Text('Siguiente',
                      style: TextStyle(color: Colors.white, fontSize: 20.0))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
