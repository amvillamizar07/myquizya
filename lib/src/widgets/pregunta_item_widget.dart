import 'package:flutter/material.dart';
import 'package:myquizya/src/models/preguntas_model.dart';
import 'package:myquizya/src/page/preguntas/pregunta_crear_page.dart';
import 'package:myquizya/src/page/preguntas/pregunta_ver_page.dart';
import 'package:myquizya/src/services/quices_service.dart';
import 'package:myquizya/src/utils/utils.dart';
import 'package:myquizya/src/widgets/item_button_widget.dart';
import 'package:provider/provider.dart';

class PreguntaItem extends StatefulWidget {
  PreguntaItem(
      {Key? key,
      required this.preguntas,
      required this.index,
      required this.search,
      this.callback})
      : super(key: key);

  int index;
  bool search;
  List<Pregunta> preguntas;
  Function? callback;

  @override
  State<PreguntaItem> createState() => _PreguntaItemState();
}

class _PreguntaItemState extends State<PreguntaItem> {
  late Pregunta _pregunta;
  @override
  Widget build(BuildContext context) {
    _pregunta = widget.preguntas[widget.index];

    final listTile = ListTile(
        title: Text('${_pregunta.enunciado}'),
        // trailing: Icon(Icons.more_vert),
        trailing: widget.search
            ? _addPregunta()
            : _actionsListQuestion(
                context: context,
                registro: _pregunta,
                index: widget.index,
              ),
        // subtitle: Text(pregunta.id.toString()),
        onTap: null);

    return widget.search
        ? listTile
        : Dismissible(
            onDismissed: (_) {
              widget.preguntas.removeAt(widget.index);
            },
            key: UniqueKey(),
            child: listTile);
  }

  // Boton de acciones , cuando estamos en modulo pregutnas
  Widget _actionsListQuestion({context, registro, index}) {
    return PopupMenuButton<ListaAcciones>(
      icon: Icon(Icons.more_vert),
      onSelected: (ListaAcciones result) {
        switch (result) {
          case ListaAcciones.show:
            Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            PreguntaVerPage(pregunta: registro)))
                .then((dynamic value) {
              if (widget.callback != null) {
                widget.callback!();
              }
            });
            break;
          case ListaAcciones.edit:
            Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            PreguntaCrearPage(main: true, pregunta: registro)))
                .then((dynamic value) {
              if (widget.callback != null) {
                widget.callback!();
              }
            });
            break;
          case ListaAcciones.initQuiz:
            break;
        }
      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<ListaAcciones>>[
        PopupMenuItem(
          value: ListaAcciones.show,
          child: ItemButtonOpcion(
            text: 'Ver',
            icon: Icons.remove_red_eye_outlined,
          ),
        ),
        PopupMenuItem<ListaAcciones>(
          value: ListaAcciones.edit,
          child: ItemButtonOpcion(
            text: 'Editar',
            icon: Icons.edit,
          ),
        ),
      ],
    );
  }

  // Boton para agregar pregunta a un quiz
  Widget _addPregunta() {
    QuicesService quicesService = Provider.of<QuicesService>(context);
    Pregunta? _resultado;
    if (quicesService.preguntasSelected.length > 0) {
      List<Pregunta> listResultados = quicesService.preguntasSelected
          .where((element) => _pregunta.id == element.id)
          .toList();

      _resultado = listResultados.length > 0 ? listResultados.first : null;
    }
    // print(_resultado);
    return IconButton(
        onPressed: () {
          setState(() {
            if (_resultado == null) {
              quicesService.addPreguntasSelected = _pregunta;
            } else {
              quicesService.preguntasSelected.removeWhere((element) {
                return _pregunta.id == element.id;
              });
            }
          });
        },
        icon: Icon(_resultado == null ? Icons.add : Icons.remove));
  }
}
