import 'package:flutter/cupertino.dart';

import 'package:myquizya/src/models/preguntas_model.dart';
export 'package:myquizya/src/models/preguntas_model.dart';

import 'package:myquizya/src/models/quices_model.dart';
export 'package:myquizya/src/models/quices_model.dart';

import 'package:myquizya/src/models/historial_quices_model.dart';
export 'package:myquizya/src/models/historial_quices_model.dart';

class QuicesService with ChangeNotifier {
  List<Quice> _quices = [
    Quice(
      id: "1",
      nombre: 'Quiz programacion I',
      materia: "programacion_i",
      tema: "patrones de diseño",
      preguntas: [
        Pregunta(
            id: "1",
            enunciado: 'Diagramas de flujo y pseudocódigos son algoritmos.',
            typePregunta: 'bool',
            puntos: 25,
            segundos: 20,
            paramsOpciones: ParamsOpciones(
              trueOpcion: '1',
              otraOpcion: false,
              opciones: <Opcione>[
                Opcione(key: '1', label: 'Verdadero', isTrue: true),
                Opcione(key: '2', label: 'Falso', isTrue: false),
              ],
            )),
        Pregunta(
            id: "2",
            enunciado: 'Lenguajes de programación',
            typePregunta: 'multiple_respuesta',
            puntos: 10,
            segundos: 15,
            paramsOpciones: ParamsOpciones(
              otraOpcion: true,
              trueOpcion: '',
              opciones: <Opcione>[
                Opcione(key: '1', label: 'Python', isTrue: true),
                Opcione(key: '2', label: 'Django', isTrue: false),
                Opcione(key: '3', label: 'PHP', isTrue: true),
                Opcione(key: '4', label: 'Laravel', isTrue: false),
              ],
            )),
        Pregunta(
            id: "3",
            enunciado: 'Un lenguaje de programación',
            typePregunta: 'unica_opcion',
            puntos: 15,
            segundos: 30,
            paramsOpciones: ParamsOpciones(
              otraOpcion: false,
              trueOpcion: '1',
              opciones: <Opcione>[
                Opcione(
                    key: '1',
                    label:
                        'Posee características propias que condicionan al pseudocódigo',
                    isTrue: false),
                Opcione(
                    key: '2',
                    label:
                        'Pueden usarse distintos aplicados a un mismo algoritmo para resolver un problema',
                    isTrue: false),
                Opcione(
                    key: '3',
                    label: 'Es incompatible con el diagrama de flujo',
                    isTrue: false),
              ],
            )),
      ],
    )
  ];

  List<Quice> get quices => _quices;

  late Quice _nuevoQuice;
  // Pregunta _nuevaPregunta = new Pregunta(
  //   paramsOpciones: ParamsOpciones(otraOpcion: false, opciones: []),
  // );

  Quice get nuevoQuice => _nuevoQuice;
  set nuevoQuice(Quice valor) {
    _nuevoQuice = valor;
    notifyListeners();
  }

  List<Pregunta> _preguntasSelected = [];
  List<Pregunta> get preguntasSelected => _preguntasSelected;
  set preguntasSelected(preguntasSelected) =>
      _preguntasSelected = preguntasSelected ?? <Pregunta>[];

  set addPreguntasSelected(Pregunta pregunta) {
    _preguntasSelected.add(pregunta);
    notifyListeners();
  }

  late HistorialQuices _presentacionQuice;
  HistorialQuices get presentacionQuice => _presentacionQuice;
  set presentacionQuice(infoQuiz) => _presentacionQuice = infoQuiz;
}
