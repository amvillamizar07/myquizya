import 'package:flutter/material.dart';

class NavigationPresentacionQuiz with ChangeNotifier {
  int _paginaActual = 0;
  int totalPages = 0;

  final PageController _pageController = PageController();

  int get paginaActual => _paginaActual;
  set paginaActual(int valor) {
    _paginaActual = valor;
    _pageController.animateToPage(valor,
        duration: const Duration(milliseconds: 250), curve: Curves.easeOut);
    notifyListeners();
  }

  PageController get pageController => _pageController;
}
