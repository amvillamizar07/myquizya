import 'package:flutter/cupertino.dart';
import 'package:myquizya/src/models/preguntas_model.dart';
export 'package:myquizya/src/models/preguntas_model.dart';

class PreguntasService with ChangeNotifier {
  // NewsServices() {}

  // PReguntas Estaticas, de base
  List<Pregunta> _preguntas = [
    Pregunta(
        id: "1",
        enunciado:
            'Dado un pseudocódigo concreto, determinará inequivocamente el tipo de lenguaje de programación a usar',
        typePregunta: 'bool',
        paramsOpciones: ParamsOpciones(
          trueOpcion: '1',
          otraOpcion: false,
          opciones: <Opcione>[
            Opcione(key: '1', label: 'Verdadero', isTrue: true),
            Opcione(key: '2', label: 'Falso', isTrue: false),
          ],
        )),
    Pregunta(
        id: "2",
        enunciado: 'Lenguajes de programación',
        typePregunta: 'multiple_respuesta',
        paramsOpciones: ParamsOpciones(
          otraOpcion: true,
          trueOpcion: '',
          opciones: <Opcione>[
            Opcione(key: '1', label: 'Python', isTrue: true),
            Opcione(key: '2', label: 'Django', isTrue: false),
            Opcione(key: '3', label: 'PHP', isTrue: true),
            Opcione(key: '4', label: 'Laravel', isTrue: false),
          ],
        )),
    Pregunta(
        id: "3",
        enunciado: 'Un lenguaje de programación',
        typePregunta: 'unica_opcion',
        paramsOpciones: ParamsOpciones(
          otraOpcion: false,
          trueOpcion: '1',
          opciones: <Opcione>[
            Opcione(
                key: '1',
                label:
                    'Posee características propias que condicionan al pseudocódigo',
                isTrue: false),
            Opcione(
                key: '2',
                label:
                    'Pueden usarse distintos aplicados a un mismo algoritmo para resolver un problema',
                isTrue: false),
            Opcione(
                key: '3',
                label: 'Es incompatible con el diagrama de flujo',
                isTrue: false),
          ],
        )),
  ];

  List<Pregunta> get preguntas => _preguntas;

  late Pregunta _nuevaPregunta;
  // Pregunta _nuevaPregunta = new Pregunta(
  //   paramsOpciones: ParamsOpciones(otraOpcion: false, opciones: []),
  // );

  Pregunta get nuevaPregunta => _nuevaPregunta;
  set nuevaPregunta(Pregunta valor) {
    _nuevaPregunta = valor;
    notifyListeners();
  }

  List<Pregunta> _resultadosSearch = [];
  List<Pregunta> get resultadosSearch => _resultadosSearch;
  buscarPeliculas(String query) {
    _resultadosSearch.clear();
    _resultadosSearch = _preguntas
        .where((element) => element.enunciado!.contains(query))
        .toList();
  }
}
