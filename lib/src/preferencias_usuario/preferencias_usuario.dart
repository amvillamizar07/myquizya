import 'package:shared_preferences/shared_preferences.dart';

class PreferenciaUsuario {
  static late final instance;

  static Future<SharedPreferences> init() async =>
      instance = await SharedPreferences.getInstance();

  String get token {
    return instance.getString('token') ?? '';
  }

  set token(String value) {
    instance.setString('token', value);
  }

  String get ultimaPagina {
    return instance.getString('ultimaPagina') ?? 'home';
  }

  set ultimaPagina(String value) {
    instance.setString('ultimaPagina', value);
  }

  // static String getString(String key, [String? defValue]) {
  //   return instance.getString(key) ?? defValue ?? "";
  // }

  // static Future<bool> setString(String key, String value) {
  //   return instance.setString(key, value);
  // }

}
