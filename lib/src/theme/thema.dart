import 'package:flutter/material.dart';

final miTema = ThemeData.light().copyWith(
  colorScheme: ColorScheme.fromSwatch().copyWith(
    primary: Color.fromRGBO(56, 146, 255, 1.0),
    secondary: Color.fromRGBO(56, 146, 255, 1.0),
    // secondary: Color.fromRGBO(245, 199, 29, 1.0),
    // background: Colors.blueAccent,
    // primaryVariant: Color.fromRGBO(56, 146, 255, 1.0),
  ),
  primaryIconTheme: IconThemeData(color: Color.fromRGBO(56, 146, 255, 1.0)),
  primaryColor: Color.fromRGBO(56, 146, 255, 1.0),
  appBarTheme: AppBarTheme(
      backgroundColor: Color.fromRGBO(56, 146, 255, 1.0),
      actionsIconTheme: IconThemeData(color: Colors.white)),
  // backgroundColor: Color.fromRGBO(23, 214, 50, 1.0),
  // bottomAppBarColor: Color.fromRGBO(56, 146, 255, 1.0),
  bottomNavigationBarTheme: BottomNavigationBarThemeData().copyWith(
      backgroundColor: Color.fromRGBO(56, 146, 255, 1.0),
      selectedItemColor: Colors.white,
      unselectedItemColor: Colors.grey),
);
