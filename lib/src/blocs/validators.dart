import 'dart:async';

class Validators {
  final validaremail =
      StreamTransformer<String, String>.fromHandlers(handleData: (data, sink) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);

    if (regExp.hasMatch(data)) {
      sink.add(data);
    } else {
      sink.addError('Correo invalido');
    }
  });

  final validarPassword =
      StreamTransformer<String, String>.fromHandlers(handleData: (data, sink) {
    if (data.length >= 6) {
      sink.add(data);
    } else {
      sink.addError('Minimo 6 caracteres');
    }
  });
}
