import 'dart:async';

import 'package:myquizya/src/blocs/validators.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc with Validators {
  // final _emailController = StreamController<String>.broadcast();
  // final _passwordController = StreamController<String>.broadcast();
  // streamcontroller con broadcast, funcioa igual que beheaviorSubjetc
  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();

  // Recuperar los datos del stream
  Stream<String> get emailStream =>
      _emailController.stream.transform(validaremail);
  Stream<String> get passwordStream =>
      _passwordController.stream.transform(validarPassword);

  Stream<bool> get formValidStream =>
      CombineLatestStream.combine2(emailStream, passwordStream, (a, b) => true);

  // Insertar valores al stream
  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passwordController.sink.add;

  // Obtener ultimo valor ingresado a los streams
  String get email => _emailController.hasValue ? _emailController.value : '';
  String get password =>
      _passwordController.hasValue ? _passwordController.value : '';

  dispose() {
    _emailController.close();
    _passwordController.close();
  }
}
