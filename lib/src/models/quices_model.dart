import 'dart:convert';
import 'package:myquizya/src/models/preguntas_model.dart';
import 'package:myquizya/src/utils/utils.dart';

List<Quice> quiceFromJson(String str) =>
    List<Quice>.from(json.decode(str).map((x) => Quice.fromJson(x)));

String quiceToJson(List<Quice> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Quice {
  Quice({
    this.id,
    this.nombre,
    this.materia,
    this.tema,
    required this.preguntas,
  });

  String? id;
  String? nombre;
  String? materia;
  String? tema;
  List<Pregunta>? preguntas;

  factory Quice.fromJson(Map<String, dynamic> json) => Quice(
        id: json["id"],
        nombre: json["nombre"],
        materia: json["materia"],
        tema: json["tema"],
        preguntas: List<Pregunta>.from(
            json["preguntas"].map((x) => Pregunta.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "materia": materia,
        "tema": tema,
        "preguntas": List<dynamic>.from(preguntas!.map((x) => x.toJson())),
      };

  String displayTotalPuntos() {
    int _totalPuntos = 0;
    this.preguntas!.forEach((element) {
      _totalPuntos += element.puntos ?? 0;
    });

    return '$_totalPuntos';
  }

  String displayDuracion() {
    int _totalSegundos = 0;
    this.preguntas!.forEach((element) {
      _totalSegundos += element.segundos ?? 0;
    });

    String minutos = formaterTiempo('minuto', _totalSegundos);
    String segundos = formaterTiempo('segundos', _totalSegundos);
    return '$minutos:$segundos';
  }
}
