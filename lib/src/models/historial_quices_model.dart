// To parse this JSON data, do
//
//     final historialQuices = historialQuicesFromJson(jsonString);

import 'dart:convert';

import 'package:myquizya/src/models/alumno_model.dart';
import 'package:myquizya/src/models/quices_model.dart';

List<HistorialQuices> historialQuicesFromJson(String str) =>
    List<HistorialQuices>.from(
        json.decode(str).map((x) => HistorialQuices.fromJson(x)));

String historialQuicesToJson(List<HistorialQuices> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HistorialQuices {
  HistorialQuices({
    this.id,
    this.grupo,
    this.codigo,
    this.alumnos,
    required this.quiz,
  });

  String? id;
  String? grupo;
  String? codigo;
  List<Alumno>? alumnos;
  Quice quiz;

  factory HistorialQuices.fromJson(Map<String, dynamic> json) =>
      HistorialQuices(
        id: json["id"],
        grupo: json["grupo"],
        codigo: json["codigo"],
        alumnos:
            List<Alumno>.from(json["alumnos"].map((x) => Alumno.fromJson(x))),
        quiz: Quice.fromJson(json["quiz"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "grupo": grupo,
        "codigo": codigo,
        "alumnos": List<dynamic>.from(alumnos!.map((x) => x.toJson())),
        "quiz": quiz.toJson(),
      };
}
