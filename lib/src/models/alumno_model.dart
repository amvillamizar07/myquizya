// To parse this JSON data, do
//
//     final alumno = alumnoFromJson(jsonString);

import 'dart:convert';

List<Alumno> alumnoFromJson(String str) =>
    List<Alumno>.from(json.decode(str).map((x) => Alumno.fromJson(x)));

String alumnoToJson(List<Alumno> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Alumno {
  Alumno({
    this.id,
    this.nombre,
    this.apellido,
    this.correo,
    this.cc,
  });

  String? id;
  String? nombre;
  String? apellido;
  String? correo;
  String? cc;

  factory Alumno.fromJson(Map<String, dynamic> json) => Alumno(
        id: json["id"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        correo: json["correo"],
        cc: json["cc"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "apellido": apellido,
        "correo": correo,
        "cc": cc,
      };
}
