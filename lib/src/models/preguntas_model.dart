// To parse this JSON data, do
//
//     final pregunta = preguntaFromJson(jsonString);
//    https://app.quicktype.io/?share=4Ik8Upww0mN33e2CBVmq

import 'dart:convert';

List<Pregunta> preguntaFromJson(String str) =>
    List<Pregunta>.from(json.decode(str).map((x) => Pregunta.fromJson(x)));

String preguntaToJson(List<Pregunta> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Pregunta {
  Pregunta({
    this.id,
    this.enunciado,
    this.typePregunta,
    this.puntos,
    this.segundos,
    required this.paramsOpciones,
  });

  String? id;
  String? enunciado;
  String? typePregunta;
  int? puntos;
  int? segundos;
  ParamsOpciones paramsOpciones;

  factory Pregunta.fromJson(Map<String, dynamic> json) => Pregunta(
        id: json["id"],
        enunciado: json["enunciado"],
        typePregunta: json["type_pregunta"],
        puntos: json["puntos"],
        segundos: json["segundos"],
        paramsOpciones: ParamsOpciones.fromJson(json["params_opciones"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "enunciado": enunciado,
        "type_pregunta": typePregunta,
        "puntos": puntos,
        "segundos": segundos,
        "params_opciones": paramsOpciones.toJson(),
      };
}

class ParamsOpciones {
  ParamsOpciones({
    required this.otraOpcion,
    this.trueOpcion,
    required this.opciones,
    this.radioChecked,
  });

  bool otraOpcion;
  dynamic trueOpcion;
  dynamic radioChecked;
  List<Opcione> opciones;

  factory ParamsOpciones.fromJson(Map<String, dynamic> json) => ParamsOpciones(
        otraOpcion: json["otra_opcion"],
        trueOpcion: json["true_opcion"],
        opciones: List<Opcione>.from(
            json["opciones"].map((x) => Opcione.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "otra_opcion": otraOpcion,
        "true_opcion": trueOpcion,
        "opciones": List<dynamic>.from(opciones.map((x) => x.toJson())),
      };
}

class Opcione {
  Opcione({
    required this.key,
    required this.label,
    required this.isTrue,
    this.checked,
  });

  String key;
  String label;
  bool isTrue;
  bool? checked;

  factory Opcione.fromJson(Map<String, dynamic> json) => Opcione(
        key: json["key"],
        label: json["label"],
        isTrue: json["is_true"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "label": label,
        "is_true": isTrue,
      };
}
