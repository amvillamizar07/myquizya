import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:myquizya/src/page/historia_quices_page.dart';
import 'package:myquizya/src/page/preguntas/preguntas_page.dart';
import 'package:myquizya/src/page/quices/quices_page.dart';

class TabsPage extends StatefulWidget {
  TabsPage({Key? key}) : super(key: key);

  static final routeName = 'tabs_page';

  @override
  State<TabsPage> createState() => _TabsPageState();
}

class _TabsPageState extends State<TabsPage> {
  // PageController controller;
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => _NavegacionModels(),
      child: Scaffold(
        body: _Paginas(),
        bottomNavigationBar: _Navegacion(),
      ),
    );
  }
}

class _Paginas extends StatelessWidget {
  _Paginas({
    Key? key,
  }) : super(key: key);

  bool init = true;

  @override
  Widget build(BuildContext context) {
    final navegacionModel = Provider.of<_NavegacionModels>(context);

    if (init) {
      init = false;
      // dynamic dataRoute = ModalRoute.of(context)?.settings.arguments;
      // if (dataRoute != null) {
      //   dataRoute = dataRoute as Map<String, dynamic>;
      //   navegacionModel.paginaActual = dataRoute['tabIndex'];
      // }
    }

    List<Widget> listPages = <Widget>[
      PreguntasPage(main: true),
      QuicesPage(),
      HistoriaQuicesPage(),
    ];

    // return PageView.builder(
    //   itemCount: listPages.length,
    //   controller: navegacionModel.pageController,
    //   physics: const NeverScrollableScrollPhysics(),
    //   itemBuilder: (context, position) {
    //     return listPages[position];
    //   },
    // );
    return PageView(
      controller: navegacionModel.pageController,
      // physics: const BouncingScrollPhysics(), // Permite deslizar pantallas
      physics: const NeverScrollableScrollPhysics(), // BLoqu el scoll
      children: listPages,
    );
  }
}

class _Navegacion extends StatelessWidget {
  const _Navegacion({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final navegacionModel = Provider.of<_NavegacionModels>(context);

    return BottomNavigationBar(
      currentIndex: navegacionModel.paginaActual,
      onTap: (i) => navegacionModel.paginaActual = i,
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.tungsten_sharp), label: 'Preguntas'),
        BottomNavigationBarItem(icon: Icon(Icons.assignment), label: 'Quices'),
        BottomNavigationBarItem(icon: Icon(Icons.history), label: 'Historial'),
      ],
    );
  }
}

class _NavegacionModels with ChangeNotifier {
  int _paginaActual = 0;

  PageController _pageController = PageController();

  int get paginaActual => _paginaActual;

  set paginaActual(int valor) {
    if (_paginaActual != valor) {
      _paginaActual = valor;
      if (_pageController.hasClients) {
        _pageController.animateToPage(valor,
            duration: const Duration(milliseconds: 250), curve: Curves.easeOut);
        notifyListeners();
      }
    }
  }

  PageController get pageController => _pageController;
}
