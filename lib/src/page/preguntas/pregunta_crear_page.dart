import 'package:flutter/material.dart';

import 'package:myquizya/src/models/preguntas_model.dart';
import 'package:myquizya/src/services/pregunstas_service.dart';
import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/widgets/form_pregunta.dart';

class PreguntaCrearPage extends StatefulWidget {
  PreguntaCrearPage({Key? key, required this.main, this.pregunta})
      : super(key: key);

  static final routeName = 'pregunta_form';

  bool main;
  Pregunta? pregunta;

  @override
  State<PreguntaCrearPage> createState() => _PregungaCrearPageState();
}

class _PregungaCrearPageState extends State<PreguntaCrearPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  // Pregunta pregunta = Pregunta(
  //     paramsOpciones: ParamsOpciones(
  //   otraOpcion: false,
  //   opciones: <Opcione>[],
  // ));

  @override
  Widget build(BuildContext context) {
    // final preguntaData = ModalRoute.of(context)?.settings.arguments;

    print(widget.pregunta);

    if (widget.pregunta == null) {
      widget.pregunta = Pregunta(
          paramsOpciones: ParamsOpciones(
        otraOpcion: false,
        opciones: <Opcione>[],
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
            widget.pregunta!.id != null ? 'Editar Pregunta' : 'Crear pregunta'),
        backgroundColor: miTema.appBarTheme.backgroundColor,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(15.0),
        child: FormPregunta(
            pregunta: widget.pregunta as Pregunta, main: widget.main),
      ),
    );
  }
}
