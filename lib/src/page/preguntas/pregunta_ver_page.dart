import 'package:flutter/material.dart';

import 'package:myquizya/src/models/preguntas_model.dart';
import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/widgets/form_pregunta.dart';

class PreguntaVerPage extends StatefulWidget {
  PreguntaVerPage({Key? key, required this.pregunta}) : super(key: key);

  static final routeName = 'pregunta_ver';

  Pregunta pregunta;

  @override
  State<PreguntaVerPage> createState() => _PregungaVerPageState();
}

class _PregungaVerPageState extends State<PreguntaVerPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final preguntaData = ModalRoute.of(context)?.settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title:
            Text(preguntaData != null ? 'Editar Pregunta' : 'Crear pregunta'),
        backgroundColor: miTema.appBarTheme.backgroundColor,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(15.0),
        child: FormPregunta(pregunta: widget.pregunta, view: true),
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     Navigator.pop(context);
      //   },
      //   child: Icon(Icons.save),
      // ),
    );
  }
}
