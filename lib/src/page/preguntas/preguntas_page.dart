import 'package:flutter/material.dart';
import 'package:myquizya/src/widgets/drawer_menu.dart';
import 'package:provider/provider.dart';

import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/widgets/pregunta_item_widget.dart';
import 'package:myquizya/src/search/search_preguntas_delegate.dart';
import 'package:myquizya/src/services/pregunstas_service.dart';
import 'package:myquizya/src/page/preguntas/pregunta_crear_page.dart';

class PreguntasPage extends StatefulWidget {
  PreguntasPage({Key? key, required this.main}) : super(key: key);

  static const routeName = 'preguntas';

  // True => Modulo Preguntas
  // False => Estan buscando preguntas para agregar al quice
  bool main;

  @override
  State<PreguntasPage> createState() => _PreguntasPageState();
}

class _PreguntasPageState extends State<PreguntasPage> {
  int id = 0;

  @override
  Widget build(BuildContext context) {
    final listPreguntas = Provider.of<PreguntasService>(context).preguntas;

    return Scaffold(
      appBar: AppBar(
        title: Text('Preguntas'),
        backgroundColor: miTema.appBarTheme.backgroundColor,
        actions: <Widget>[
          IconButton(
              onPressed: () {
                showSearch(
                    context: context,
                    delegate: DataPreguntasSearch(),
                    query: '',
                    useRootNavigator: true);
              },
              icon: Icon(Icons.search)),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: _crearListado(listPreguntas),
      ),
      floatingActionButton: _floatingActionButton(context),
      drawer: DrawerMenu(),
    );
  }

  Widget _crearListado(listPreguntas) {
    return ListView.builder(
      itemCount: listPreguntas.length,
      itemBuilder: (context, i) => PreguntaItem(
          index: i,
          preguntas: listPreguntas,
          search: !widget.main,
          callback: () {
            refreshData();
            setState(() {});
          }),
    );
  }

  Widget _floatingActionButton(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        if (widget.main) {
          Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PreguntaCrearPage(main: true)))
              .then((dynamic value) {
            setState(() {
              refreshData();
            });
          });
        } else {
          Navigator.of(context).pop();
        }
      },
      // backgroundColor: Colors.deepPurple,
      child: Icon(widget.main ? Icons.add : Icons.save),
    );
  }

  void refreshData() {
    id++;
  }
}
