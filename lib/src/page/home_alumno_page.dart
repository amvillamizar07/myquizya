import 'package:flutter/material.dart';
import 'package:myquizya/src/page/historial_alum_page.dart';
import 'package:myquizya/src/page/perfil_page.dart';
import 'package:myquizya/src/page/quices/alumno/init_quice_alum_page.dart';
import 'package:myquizya/src/widgets/logo_wiget.dart';

class HomeAlumnoPage extends StatefulWidget {
  HomeAlumnoPage({Key? key}) : super(key: key);

  static const String routeName = 'home_alumno_page';

  @override
  _HomeAlumnoPageState createState() => _HomeAlumnoPageState();
}

class _HomeAlumnoPageState extends State<HomeAlumnoPage> {
  String codigo = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.contacts_rounded),
          onPressed: () {
            Navigator.pushNamed(context, PerfilPage.routeName);
          },
        ),
      ),
      body: _homePage(context),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, HistorialAlu.routeName);
        },
        child: Icon(Icons.history_edu),
      ),
    );
  }

  _homePage(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: <Widget>[
          Logo(),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _formQuiz(context),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _formQuiz(BuildContext context) {
    return Center(
      child: Column(
        children: [
          _inputCodigo(),
          SizedBox(height: 20.0),
          _iniciarBoton(context),
        ],
      ),
    );
  }

  _inputCodigo() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: TextField(
          keyboardType: TextInputType.text,
          textAlign: TextAlign.center,
          decoration: InputDecoration(
            icon: Icon(Icons.code),
            hintText: 'Ingrese el código',
            labelText: 'Ingrese el código',
          ),
          onChanged: (value) {
            setState(() {
              codigo = value.toString();
            });
          }),
    );
  }

  _iniciarBoton(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(
      elevation: 0.0,
      textStyle: const TextStyle(fontSize: 20),
    );

    return ElevatedButton(
        onPressed: codigo.length > 0 ? () => _iniciar(context) : null,
        style: style,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
          child: Text('Ingresa'),
        ));
  }

  _iniciar(BuildContext context) {
    Navigator.pushNamed(context, InitQuiceAlumPage.routeName);
  }
}
