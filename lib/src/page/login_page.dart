import 'package:flutter/material.dart';
import 'package:myquizya/src/page/home_alumno_page.dart';

import 'package:myquizya/src/page/registro_page.dart';
import 'package:myquizya/src/page/tabs_page.dart';
import 'package:myquizya/src/services/usuario_service.dart';
import 'package:myquizya/src/theme/thema.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  static final String routeName = 'login';

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String correo = '';
  String password = '';
  String tipoUsuario = '1';
  late UsuarioService usuarioService;

  @override
  Widget build(BuildContext context) {
    usuarioService = Provider.of<UsuarioService>(context);
    return Scaffold(
      body: Stack(
        children: [_crearFondo(context), _loginForm(context)],
      ),
    );
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final fondoMorado = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        miTema.colorScheme.primary,
        miTema.colorScheme.secondary,
      ])),
    );

    final circulo = Container(
        width: 100.0,
        height: 100.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05),
        ));
    return Stack(
      children: <Widget>[
        fondoMorado,
        Positioned(child: circulo, top: (size.height * 0.4) * 0.5, left: 30.0),
        Positioned(child: circulo, top: -40.0, right: -30.0),
        Positioned(child: circulo, bottom: -50.0, right: -10.0),
        Positioned(child: circulo, bottom: 120.0, right: 20.0),
        Positioned(child: circulo, bottom: -50.0, left: -20.0),
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(Icons.person_pin_circle, color: Colors.white, size: 100.0),
              SizedBox(
                height: 10.0,
                width: double.infinity,
              ),
              Text(
                'MyQuizYa',
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _loginForm(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
              child: Container(
            height: 180.0,
          )),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 3.0,
                  offset: Offset(0.0, 5.0),
                  spreadRadius: 3.0,
                ),
              ],
            ),
            child: Form(
              child: Column(
                children: <Widget>[
                  Text(
                    'Ingreso',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  SizedBox(height: 60.0),
                  _crearEmail(),
                  SizedBox(height: 20.0),
                  _crearPassword(),
                  SizedBox(height: 15.0),
                  _tipoUsuario(),
                  SizedBox(height: 15.0),
                  _crearBoton(context),
                ],
              ),
            ),
          ),
          TextButton(
              onPressed: () => Navigator.pushReplacementNamed(
                  context, RegistroPage.routeName),
              child: Text('Crear una cuenta')),
          SizedBox(height: 100.0),
        ],
      ),
    );
  }

  Widget _crearEmail() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(Icons.alternate_email),
          hintText: 'ejemplo@correo.com',
          labelText: 'Correo electronico',
          counterText: correo,
        ),
        onChanged: (value) => correo = value.toString(),
      ),
    );
  }

  Widget _crearPassword() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: TextField(
        keyboardType: TextInputType.visiblePassword,
        obscureText: true,
        decoration: InputDecoration(
          icon: Icon(Icons.lock),
          labelText: 'Contraseña',
          counterText: password,
        ),
        onChanged: (value) => password = value,
      ),
    );
  }

  Widget _tipoUsuario() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Row(
          children: [
            Radio(
              value: '1',
              groupValue: tipoUsuario,
              onChanged: (value) {
                setState(() {
                  tipoUsuario = value.toString();
                });
              },
            ),
            Text("Docente")
          ],
        ),
        Row(
          children: [
            Radio(
              value: '2',
              groupValue: tipoUsuario,
              onChanged: (value) {
                setState(() {
                  tipoUsuario = value.toString();
                });
              },
            ),
            Text("Alumno")
          ],
        ),
      ],
    );
  }

  Widget _crearBoton(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(
      elevation: 0.0,
      textStyle: const TextStyle(fontSize: 20),
    );

    return ElevatedButton(
        onPressed: correo.length > 0 && password.length > 0
            ? () => _login(context)
            : null,
        style: style,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
          child: Text('Ingresa'),
        ));
  }

  _login(BuildContext context) async {
    usuarioService.tipoUsuario = tipoUsuario;
    switch (usuarioService.tipoUsuario) {
      case "1":
        Navigator.restorablePushNamedAndRemoveUntil(
            context, TabsPage.routeName, (route) => false);
        break;
      default:
        Navigator.restorablePushNamedAndRemoveUntil(
            context, HomeAlumnoPage.routeName, (route) => false);
    }
  }
}
