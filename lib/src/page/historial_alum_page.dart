import 'package:flutter/material.dart';
import 'package:myquizya/src/page/quices/alumno/detalle_quiz_alum_page.dart';

class HistorialAlu extends StatefulWidget {
  HistorialAlu({Key? key}) : super(key: key);

  static const routeName = 'historial_alu_page';

  @override
  _HistorialAluState createState() => _HistorialAluState();
}

class _HistorialAluState extends State<HistorialAlu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historial de quices'),
      ),
      body: _page(),
    );
  }

  Widget _page() {
    return SingleChildScrollView(
      child: Column(
        children: [
          _listaParticipantes(),
        ],
      ),
    );
  }

  Widget _listaParticipantes() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: 20.0),
      child: DataTable(
        columns: [
          DataColumn(
            label: Text('Código'),
          ),
          DataColumn(
            label: Text('Fecha'),
          ),
          DataColumn(
            label: Text('Acción'),
          ),
        ],
        rows: [
          _itemLista('HN87IO', '15/03/2021'),
          _itemLista('AAAUI8', '07/03/2021'),
          _itemLista('89HYT0', '18/05/2021'),
          _itemLista('QAO809', '07/05/2021'),
          _itemLista('9JJ8CA', '07/08/2021'),
          _itemLista('6C6HHA', '07/09/2021'),
          _itemLista('1AX7XV', '28/10/2021'),
          _itemLista('1LO9NZ', '10/11/2021'),
          _itemLista('TC621H', '15/11/2021'),
        ],
      ),
    );
  }

  DataRow _itemLista(String codigo, String fecha) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(codigo)),
        DataCell(Text(fecha)),
        DataCell(IconButton(
            onPressed: () {
              Navigator.pushNamed(context, DetalleQuizAlum.routeName);
            },
            icon: Icon(Icons.remove_red_eye_outlined,
                color: Colors.greenAccent))),
      ],
    );
  }
}
