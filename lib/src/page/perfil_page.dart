import 'package:flutter/material.dart';
import 'package:myquizya/src/page/login_page.dart';
import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/utils/utils.dart';

class PerfilPage extends StatefulWidget {
  const PerfilPage({Key? key}) : super(key: key);

  static const routeName = 'page_perfil';

  @override
  _PerfilPageState createState() => _PerfilPageState();
}

class _PerfilPageState extends State<PerfilPage> {
  String nombre = 'Andres Villamizar';
  String correo = 'avillamizar15@udi.edu.co';
  String documento = '11020000000';
  String password = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                Navigator.restorablePushNamed(context, LoginPage.routeName);
              },
              icon: Icon(Icons.exit_to_app_outlined))
        ],
      ),
      body: _body(),
    );
  }

  _body() {
    return Stack(
      children: [
        _crearFondo(context),
        _perfilDatos(context),
      ],
    );
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final fondoMorado = Container(
      height: size.height * 1,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        miTema.colorScheme.primary,
        miTema.colorScheme.secondary,
      ])),
    );

    final circulo = Container(
        width: 100.0,
        height: 100.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05),
        ));
    return Stack(
      children: <Widget>[
        fondoMorado,
        Positioned(child: circulo, top: (size.height * 0.4) * 0.5, left: 30.0),
        Positioned(child: circulo, top: -40.0, right: -30.0),
        Positioned(child: circulo, bottom: -50.0, right: -10.0),
        Positioned(child: circulo, bottom: 120.0, right: 20.0),
        Positioned(child: circulo, bottom: -50.0, left: -20.0),
        Container(
          padding: EdgeInsets.only(top: 40.0),
          child: Column(
            children: <Widget>[
              Icon(Icons.person_outline_sharp, color: Colors.white, size: 80.0),
              SizedBox(
                height: 10.0,
                width: double.infinity,
              ),
              Text(
                'Perfil',
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _perfilDatos(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
              child: Container(
            height: 150.0,
          )),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 3.0,
                  offset: Offset(0.0, 5.0),
                  spreadRadius: 3.0,
                ),
              ],
            ),
            child: Form(
              child: Column(
                children: <Widget>[
                  Text(
                    'Ingreso',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  SizedBox(height: 60.0),
                  _crearNombre(),
                  SizedBox(height: 20.0),
                  _crearDocumento(),
                  SizedBox(height: 20.0),
                  _crearEmail(),
                  SizedBox(height: 20.0),
                  _crearBoton(context),
                ],
              ),
            ),
          ),
          SizedBox(height: 100.0),
        ],
      ),
    );
  }

  Widget _crearNombre() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: TextFormField(
        initialValue: nombre,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Nombre',
        ),
        onChanged: (value) => nombre = value.toString(),
      ),
    );
  }

  Widget _crearDocumento() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: TextFormField(
        initialValue: documento,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          icon: Icon(Icons.contact_mail),
          labelText: 'Número de documento',
        ),
        onChanged: (value) => documento = value.toString(),
      ),
    );
  }

  Widget _crearEmail() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: TextFormField(
        initialValue: correo,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          icon: Icon(Icons.alternate_email),
          hintText: 'ejemplo@correo.com',
          labelText: 'Correo electronico',
        ),
        onChanged: (value) => correo = value.toString(),
      ),
    );
  }

  Widget _crearBoton(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(
      elevation: 0.0,
      textStyle: const TextStyle(fontSize: 20),
    );

    return ElevatedButton(
        onPressed:
            correo.length > 0 && documento.length > 0 && nombre.length > 0
                ? () => mostrarAlerta(
                    context: context,
                    title: 'Exito',
                    mensaje: 'Datos actualizados')
                : null,
        style: style,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
          child: Text('Guardar'),
        ));
  }
}
