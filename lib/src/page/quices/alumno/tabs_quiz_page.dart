import 'package:flutter/material.dart';
import 'package:myquizya/src/services/presentacion_quiz_service.dart';
import 'package:myquizya/src/widgets/resultados_initermedio_widget.dart';
import 'package:provider/provider.dart';

import 'package:myquizya/src/services/quices_service.dart';
import 'package:myquizya/src/widgets/pregunta_presentacion_widget.dart';

class TabsQuizPage extends StatefulWidget {
  const TabsQuizPage({Key? key}) : super(key: key);

  static final routeName = 'tabs_quiz_page';

  @override
  State<TabsQuizPage> createState() => _TabsQuizPageState();
}

class _TabsQuizPageState extends State<TabsQuizPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => NavigationPresentacionQuiz(),
      child: Scaffold(
        body: _Paginas(),
      ),
    );
  }
}

class _Paginas extends StatelessWidget {
  _Paginas({Key? key}) : super(key: key);

  late QuicesService quicesService;
  late HistorialQuices historialQuices;

  @override
  Widget build(BuildContext context) {
    final navegacionModel = Provider.of<NavigationPresentacionQuiz>(context);

    quicesService = Provider.of<QuicesService>(context);
    historialQuices = quicesService.presentacionQuice;
    final preguntas = historialQuices.quiz.preguntas;

    List<Widget> _listaPaginas = [];

    if (preguntas != null && preguntas.length > 0) {
      int contador = 0;
      bool respuesta = true;
      preguntas.forEach((element) {
        _listaPaginas.add(PreguntaPresentacion(pregunta: element, i: contador));
        contador++;
        _listaPaginas
            .add(ResultadosIntermedios(i: contador, respuesta: respuesta));
        contador++;
        respuesta = !respuesta;
      });
    }

    navegacionModel.totalPages = _listaPaginas.length - 1;

    return PageView(
      controller: navegacionModel.pageController,

      // physics: const BouncingScrollPhysics(), // Permite deslizar pantallas
      physics: const NeverScrollableScrollPhysics(), // BLoqu el scoll
      children: _listaPaginas,
    );
  }
}
