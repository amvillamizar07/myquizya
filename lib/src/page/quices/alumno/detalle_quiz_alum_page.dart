import 'package:flutter/material.dart';
import 'package:myquizya/src/theme/thema.dart';

class DetalleQuizAlum extends StatefulWidget {
  DetalleQuizAlum({Key? key}) : super(key: key);

  static const routeName = 'detalle_quiz_alum';

  @override
  _DetalleQuizAlumState createState() => _DetalleQuizAlumState();
}

class _DetalleQuizAlumState extends State<DetalleQuizAlum> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detalle del quiz'),
      ),
      body: _page(),
    );
  }

  Widget _page() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            _nombreQuice(),
            _nombreDocente(),
            _fechaPresentacion(),
            _listaResultados(),
          ],
        ),
      ),
    );
  }

  Widget _nombreQuice() {
    return TextFormField(
      initialValue: 'Modelado de datos',
      enabled: false,
      decoration: InputDecoration(labelText: 'Nombre quiz'),
    );
  }

  Widget _nombreDocente() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Text(
        'Docente: Carlos Eduardo Perez',
        style: TextStyle(fontSize: 20.0),
        textAlign: TextAlign.left,
      ),
    );
  }

  Widget _fechaPresentacion() {
    final textStyleItems = TextStyle(fontSize: 18.0);
    return Container(
      child: Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: [
          TableRow(
            children: [
              Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('Fecha: 07/04/2021', style: textStyleItems)),
              Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('Puntos: 70/100', style: textStyleItems)),
            ],
          ),
          TableRow(
            children: [
              Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('Correctas: 15/20', style: textStyleItems)),
              Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: Text('Sin respuesta: 1/20', style: textStyleItems)),
            ],
          ),
        ],
      ),
    );
  }

  Widget _listaResultados() {
    return Column(
      children: [
        SizedBox(height: 15.0),
        Text('Preguntas',
            style:
                TextStyle(color: miTema.colorScheme.primary, fontSize: 25.0)),
        ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 10.0),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('1. Un lenguaje de programación'),
                Text('Puntos: 15'),
              ],
            ),
            // trailing: Icon(Icons.more_vert),
            trailing: _resultadoRespuesta(true)),
        ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 10.0),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    '2. Un lenguaje de programación conecta a personas y ordenadores'),
                Text('Puntos: 10'),
              ],
            ),
            // trailing: Icon(Icons.more_vert),
            trailing: _resultadoRespuesta(true)),
        ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 10.0),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    '3. Dado un pseudocódigo concreto, determinará inequivocamente el tipo de lenguaje de programación a usar'),
                Text('Puntos: 15'),
              ],
            ),
            // trailing: Icon(Icons.more_vert),
            trailing: _resultadoRespuesta(false)),
        ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 10.0),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('4. En la construcción de un diagrama de flujo'),
                Text('Puntos: 20'),
              ],
            ),
            // trailing: Icon(Icons.more_vert),
            trailing: _resultadoRespuesta(true)),
        ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 10.0),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('5. Un diagrama de flujo'),
                Text('Puntos: 10'),
              ],
            ),
            // trailing: Icon(Icons.more_vert),
            trailing: _resultadoRespuesta(false)),
        ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 10.0),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                    '6. Las aplicaciones Java se encuentran compiladas en un bytecode'),
                Text('Puntos: 10'),
              ],
            ),
            // trailing: Icon(Icons.more_vert),
            trailing: _resultadoRespuesta(true)),
        ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 10.0),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('7. ¿Que es un operador?'),
                Text('Puntos: 10'),
              ],
            ),
            // trailing: Icon(Icons.more_vert),
            trailing: _resultadoRespuesta(true)),
      ],
    );
  }

  Widget _resultadoRespuesta(bool respuesta) {
    return SizedBox.fromSize(
      size: Size(30, 30),
      child: ClipOval(
        child: Material(
          color: respuesta ? Colors.greenAccent[400] : Colors.redAccent[400],
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  respuesta ? Icons.check : Icons.close,
                  color: Colors.white,
                  size: 25.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
