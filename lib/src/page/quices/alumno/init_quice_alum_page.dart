import 'dart:async';

import 'package:flutter/material.dart';
import 'package:myquizya/src/page/quices/alumno/tabs_quiz_page.dart';
import 'package:myquizya/src/widgets/logo_wiget.dart';

import 'dart:ui';
import 'package:provider/provider.dart';

import 'package:myquizya/src/services/quices_service.dart';

class InitQuiceAlumPage extends StatefulWidget {
  const InitQuiceAlumPage({Key? key}) : super(key: key);

  static const routeName = 'init_quice_alum_page';

  @override
  State<InitQuiceAlumPage> createState() => _InitQuiceAlumPageState();
}

class _InitQuiceAlumPageState extends State<InitQuiceAlumPage> {
  late QuicesService quicesService;
  bool init = true;

  @override
  Widget build(BuildContext context) {
    quicesService = Provider.of<QuicesService>(context);

    // quicesService.preguntasSelected = quiceet.quice.preguntas;
    quicesService.presentacionQuice =
        new HistorialQuices(quiz: quicesService.quices.first, grupo: 'A8');

    if (init) {
      Timer(Duration(seconds: 7), () {
        Navigator.pushReplacementNamed(context, TabsQuizPage.routeName);
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Comenzar quiz'),
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Logo(),
            Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _btnOkey(),
                    _dataQuiz(),
                  ]),
            ),
            // _listaParticipantes()
          ],
        ),
      ),
    );
  }

  Widget _btnOkey() {
    return SizedBox.fromSize(
      size: Size(100, 100),
      child: ClipOval(
        child: Material(
          color: Color.fromRGBO(56, 146, 255, 1.0),
          child: InkWell(
            splashColor: Colors.blueAccent,
            onTap: () {
              Navigator.pushNamed(context, TabsQuizPage.routeName);
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.check,
                  color: Colors.white,
                  size: 80.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _dataQuiz() {
    TextStyle textdetail = TextStyle(
        color: Colors.black,
        fontSize: 18.0,
        textBaseline: TextBaseline.alphabetic);
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: [
          Text('Docente', style: textdetail),
          Text(
            'Carlos Eduardo Perez',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 32.0,
                textBaseline: TextBaseline.alphabetic),
          ),
          Text(
              'Duración (minutos): ${quicesService.presentacionQuice.quiz.displayDuracion()}',
              style: textdetail),
          Text('Grupo: ${quicesService.presentacionQuice.grupo}',
              style: textdetail),
          SizedBox(height: 20.0),
          Icon(Icons.timer_rounded, size: 60.0)
        ],
      ),
    );
  }
}
