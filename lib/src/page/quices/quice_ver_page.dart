import 'package:flutter/material.dart';

import 'package:myquizya/src/models/quices_model.dart';
import 'package:myquizya/src/services/quices_service.dart';
import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/widgets/form_quice.dart';
import 'package:provider/provider.dart';

class QuiceVerPage extends StatefulWidget {
  QuiceVerPage({Key? key, required this.quice}) : super(key: key);

  static final routeName = 'pregunta_ver';

  Quice quice;

  @override
  State<QuiceVerPage> createState() => _QuiceVerPageState();
}

class _QuiceVerPageState extends State<QuiceVerPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  // Quice quice = Quice(preguntas: []);

  late QuicesService quicesService;

  @override
  Widget build(BuildContext context) {
    quicesService = Provider.of<QuicesService>(context);
    quicesService.preguntasSelected = widget.quice.preguntas;

    return Scaffold(
      appBar: AppBar(
        title: Text('Quiz'),
        backgroundColor: miTema.appBarTheme.backgroundColor,
      ),
      body: FormQuice(quice: widget.quice, view: true),
    );
  }
}
