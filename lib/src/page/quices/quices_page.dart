import 'package:flutter/material.dart';
import 'package:myquizya/src/page/quices/docente/quice_iniciar_page.dart';
import 'package:myquizya/src/page/quices/quice_ver_page.dart';
import 'package:myquizya/src/page/quices/quice_crear_page.dart';
import 'package:myquizya/src/utils/utils.dart';
import 'package:myquizya/src/widgets/item_button_widget.dart';
import 'package:provider/provider.dart';

import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/services/quices_service.dart';

class QuicesPage extends StatefulWidget {
  const QuicesPage({Key? key}) : super(key: key);

  @override
  State<QuicesPage> createState() => _QuicesPageState();
}

class _QuicesPageState extends State<QuicesPage> {
  int id = 0;
  late QuicesService quicesService;
  late List<Quice> listPreguntas;

  @override
  Widget build(BuildContext context) {
    quicesService = Provider.of<QuicesService>(context);
    listPreguntas = quicesService.quices;

    return Scaffold(
      appBar: AppBar(
        title: Text('Quices'),
        backgroundColor: miTema.appBarTheme.backgroundColor,
        actions: <Widget>[
          IconButton(
              onPressed: () {
                // showSearch(context: context, delegate: DataSearch(), query: '');
              },
              icon: Icon(Icons.search)),
        ],
      ),
      body: _crearListado(),
      floatingActionButton: _crearBoton(context),
    );
  }

  Widget _crearListado() {
    return ListView.builder(
      itemCount: listPreguntas.length,
      itemBuilder: (context, i) => _itemList(context, i),
    );
  }

  Widget _crearBoton(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(context,
                MaterialPageRoute(builder: (context) => QuiceCrearPage()))
            .then((dynamic value) {
          setState(() {
            refreshData();
          });
        });
      },
      // backgroundColor: Colors.deepPurple,
      child: Icon(Icons.add),
    );
  }

  Widget _itemList(BuildContext context, int index) {
    Quice quice = listPreguntas[index];
    return Dismissible(
      onDismissed: (_) {
        setState(() {
          quicesService.quices.removeAt(index);
        });
      },
      key: UniqueKey(),
      child: ListTile(
        title: Text('${quice.nombre}'),
        trailing: _actionsListQuestion(
          context: context,
          registro: quice,
          index: index,
        ),
        // subtitle: Text(pregunta.id.toString()),
      ),
    );
  }

  Widget _actionsListQuestion(
      {required BuildContext context, required Quice registro, index}) {
    return PopupMenuButton<ListaAcciones>(
      icon: Icon(Icons.more_vert),
      onSelected: (ListaAcciones result) {
        switch (result) {
          case ListaAcciones.show:
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => QuiceVerPage(
                          quice: registro,
                        ))).then((dynamic value) {
              refreshData();
              setState(() {});
            });
            break;
          case ListaAcciones.edit:
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => QuiceCrearPage(
                          quice: registro,
                        ))).then((dynamic value) {
              refreshData();
              setState(() {});
            });
            break;
          case ListaAcciones.initQuiz:
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => QuiceIniciarPage(
                          quice: registro,
                        ))).then((dynamic value) {
              refreshData();
              setState(() {});
            });
            break;
        }
      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<ListaAcciones>>[
        PopupMenuItem(
          value: ListaAcciones.initQuiz,
          child: ItemButtonOpcion(
            text: 'Iniciar quiz',
            icon: Icons.play_circle_outline_rounded,
          ),
        ),
        PopupMenuItem(
          value: ListaAcciones.show,
          child: ItemButtonOpcion(
            text: 'Ver',
            icon: Icons.remove_red_eye_outlined,
          ),
        ),
        PopupMenuItem<ListaAcciones>(
          value: ListaAcciones.edit,
          child: ItemButtonOpcion(
            text: 'Editar',
            icon: Icons.edit,
          ),
        ),
      ],
    );
  }

  void refreshData() {
    id++;
  }
}
