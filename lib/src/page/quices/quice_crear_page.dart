import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:myquizya/src/models/preguntas_model.dart';
import 'package:myquizya/src/page/preguntas/pregunta_crear_page.dart';

import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/utils/utils.dart';
import 'package:myquizya/src/widgets/form_quice.dart';
import 'package:myquizya/src/services/quices_service.dart';

import 'package:myquizya/src/page/preguntas/preguntas_page.dart';
import 'package:provider/provider.dart';

class QuiceCrearPage extends StatefulWidget {
  QuiceCrearPage({Key? key, this.quice}) : super(key: key);

  static final routeName = 'quice_ver';

  Quice? quice;

  @override
  State<QuiceCrearPage> createState() => _QuiceCrearPageState();
}

class _QuiceCrearPageState extends State<QuiceCrearPage> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  Quice quice = Quice(preguntas: <Pregunta>[]);

  int id = 0;
  late QuicesService quicesService;

  @override
  Widget build(BuildContext context) {
    dynamic quiceData = widget.quice;
    quicesService = Provider.of<QuicesService>(context);

    if (quiceData != null) {
      quice = quiceData as Quice;
      quicesService.preguntasSelected = quice.preguntas;
    } else {
      quicesService.preguntasSelected = quice.preguntas;
    }

    List<Quice> listQuices = quicesService.quices;

    return Scaffold(
      appBar: AppBar(
        title: Text(quiceData != null ? 'Editar quiz' : 'Crear quiz'),
        backgroundColor: miTema.appBarTheme.backgroundColor,
        actions: <Widget>[
          IconButton(
              onPressed: () {
                quice.preguntas = quicesService.preguntasSelected;
                if (quice.id != null) {
                  int indexEditado = listQuices
                      .lastIndexWhere((element) => element.id == quice.id);
                  quicesService.quices[indexEditado] = quice;
                } else {
                  quice.id = UniqueKey().toString();
                  quicesService.quices.add(quice);
                }
                setState(() {
                  mostrarAlerta(
                      context: context,
                      title: 'Listo',
                      mensaje: 'Quice guarda');
                });
              },
              icon: Icon(Icons.save))
        ],
      ),
      body: FormQuice(quice: quice),
      floatingActionButton: SpeedDial(
        icon: Icons.keyboard_arrow_up_sharp,
        // animatedIcon: AnimatedIcons.menu_close,
        // overlayOpacity: 0.6,
        overlayColor: Colors.black,
        children: [
          SpeedDialChild(
              child: Icon(Icons.add_link_outlined),
              label: 'Importar pregunta',
              onTap: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PreguntasPage(main: false)))
                    .then((dynamic value) {
                  refreshData();
                  setState(() {});
                });
              }),
          SpeedDialChild(
              child: Icon(Icons.add),
              label: 'Nueva pregunta',
              onTap: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                PreguntaCrearPage(main: false)))
                    .then((dynamic value) {
                  refreshData();
                  setState(() {});
                });
              }),
        ],
      ),
    );
  }

  void refreshData() {
    id++;
  }
}
