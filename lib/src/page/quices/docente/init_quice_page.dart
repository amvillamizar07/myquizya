import 'package:flutter/material.dart';
import 'package:myquizya/src/page/quices/alumno/tabs_quiz_page.dart';
import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/widgets/logo_wiget.dart';

import 'dart:ui';
import 'package:provider/provider.dart';

import 'package:myquizya/src/services/quices_service.dart';

class InitQuicePage extends StatefulWidget {
  const InitQuicePage({Key? key}) : super(key: key);

  static const routeName = 'init_quice_page';

  @override
  State<InitQuicePage> createState() => _InitQuicePageState();
}

class _InitQuicePageState extends State<InitQuicePage> {
  late QuicesService quicesService;

  @override
  Widget build(BuildContext context) {
    quicesService = Provider.of<QuicesService>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Comenzar quiz'),
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Logo(),
            SingleChildScrollView(
              child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 150.0),
                      Container(
                        padding: EdgeInsets.all(20.0),
                        child: Text(
                          quicesService.presentacionQuice.codigo.toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 42.0,
                              textBaseline: TextBaseline.alphabetic),
                        ),
                      ),
                      _btnPlay(),
                      _listaParticipantes()
                    ]),
              ),
            ),
            // _listaParticipantes()
          ],
        ),
      ),
    );
  }

  Widget _btnPlay() {
    return SizedBox.fromSize(
      size: Size(100, 100),
      child: ClipOval(
        child: Material(
          color: Color.fromRGBO(56, 146, 255, 1.0),
          child: InkWell(
            splashColor: Colors.blueAccent,
            onTap: () {
              Navigator.pushNamed(context, TabsQuizPage.routeName);
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.play_arrow,
                  color: Colors.white,
                  size: 40.0,
                ),
                Text("Comenzar", style: TextStyle(color: Colors.white))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _listaParticipantes() {
    return Container(
      padding: EdgeInsets.only(top: 20.0),
      child: Table(
        children: [
          TableRow(
            children: [
              _crearBotonRedondeado(Icons.person_outline, 'Andres Villamizar'),
              _crearBotonRedondeado(Icons.person_outline, 'Juan valrdez'),
            ],
          ),
          TableRow(
            children: [
              _crearBotonRedondeado(Icons.person_outline, 'laudy rondon'),
              _crearBotonRedondeado(Icons.person_outline, 'Sandra Cruz'),
            ],
          ),
          TableRow(
            children: [
              _crearBotonRedondeado(Icons.person_outline, 'Carlos villagran'),
              _crearBotonRedondeado(Icons.person_outline, 'Felipe Camargo'),
            ],
          ),
        ],
      ),
    );
  }

  Widget _crearBotonRedondeado(IconData icon, String text) {
    // En android es necesario usar ClipRect para q el blur funcione correctamente
    return ClipRect(
      child: Container(
        height: 60.0,
        padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.only(right: 10.0, left: 10.0, bottom: 10.0),
        decoration: BoxDecoration(
            color: Color.fromRGBO(62, 66, 107, 0.2),
            borderRadius: BorderRadius.circular(4.0)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            CircleAvatar(
              backgroundColor: miTema.primaryIconTheme.color,
              radius: 20.0,
              child: Icon(icon, color: Colors.white, size: 20.0),
            ),
            SizedBox(width: 10.0),
            Text(
              text,
              style: TextStyle(color: Colors.black),
            ),
          ],
        ),
      ),
    );
  }
}
