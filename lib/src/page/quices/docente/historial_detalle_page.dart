import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:myquizya/src/page/quices/alumno/detalle_quiz_alum_page.dart';

class HistorialDetallePage extends StatefulWidget {
  HistorialDetallePage({Key? key}) : super(key: key);

  static const routeName = 'historial_detail_page';

  @override
  HhistoriaDdetallPpageState createState() => HhistoriaDdetallPpageState();
}

class HhistoriaDdetallPpageState extends State<HistorialDetallePage> {
  final textStyle = TextStyle(fontSize: 32.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Informacion del quiz'),
      ),
      body: _page(),
    );
  }

  Widget _page() {
    return SingleChildScrollView(
      child: Column(
        children: [
          _detalleQuiz(),
          _listaParticipantes(),
          _exportData(),
        ],
      ),
    );
  }

  Widget _detalleQuiz() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: Column(
        children: [_nombreQuice(), _grupoQuiz()],
      ),
    );
  }

  Widget _nombreQuice() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Text('Quiz de progrmacion I', style: textStyle),
    );
  }

  Widget _grupoQuiz() {
    return Container(
      child: Column(
        children: [
          Table(
            children: [
              TableRow(children: [
                Text(
                  'Grupo: A10',
                  style: textStyle,
                ),
                Text(
                  'Fecha: 09/08/2021',
                  style: textStyle,
                )
              ]),
            ],
          ),
          Divider(height: 10.0),
          Table(
            children: [
              TableRow(children: [
                Text(
                  'Total Alumnos: 20',
                  style: textStyle,
                ),
              ]),
            ],
          ),
        ],
      ),
    );
  }

  Widget _listaParticipantes() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        width: MediaQuery.of(context).size.width,
        // padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
        child: DataTable(
          columns: [
            DataColumn(
              label: Text('Alumno'),
            ),
            // DataColumn(
            //   label: Text('R. Correctas'),
            // ),
            // DataColumn(
            //   label: Text('Sin Respuesta'),
            // ),
            DataColumn(label: Text('Puntos')),
            DataColumn(label: Text('Acciones')),
          ],
          rows: [
            _itemLista(
                'Andres Villamizar Villamizar', '15/20', '1/20', '75/100'),
            _itemLista('Pepito Perez', '15/20', '1/20', '75/100'),
            _itemLista('Laudy Rondon', '15/20', '1/20', '75/100'),
            _itemLista('Jeferson Ardila', '15/20', '1/20', '75/100'),
            _itemLista('Jhonson Herrera', '15/20', '1/20', '75/100'),
            _itemLista('Sandra Chinome', '15/20', '1/20', '75/100'),
            _itemLista('Nathaly Villamizar', '15/20', '1/20', '75/100'),
          ],
        ),
      ),
    );
  }

  DataRow _itemLista(
      String alumno, String correctas, String sinRespuesta, String puntos) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(alumno)),
        // DataCell(Text(correctas)),
        // DataCell(Text(sinRespuesta)),
        DataCell(Text(puntos)),
        DataCell(IconButton(
          icon: Icon(Icons.remove_red_eye_outlined),
          onPressed: () {
            Navigator.pushNamed(context, DetalleQuizAlum.routeName);
          },
        )),
      ],
    );
  }

  Widget _exportData() {
    return SizedBox.fromSize(
      size: Size(50, 50),
      child: ClipOval(
        child: Material(
          color: Color.fromRGBO(6, 195, 0, 1.0),
          child: InkWell(
            splashColor: Colors.blueAccent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.file_download,
                  color: Colors.white,
                  size: 35.0,
                ),
                // Text('Descargar',
                //     style: TextStyle(fontSize: 15.0, color: Colors.white)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
