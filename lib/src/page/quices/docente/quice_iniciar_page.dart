import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:myquizya/src/models/quices_model.dart';
import 'package:myquizya/src/services/quices_service.dart';
import 'package:myquizya/src/theme/thema.dart';
import 'package:myquizya/src/utils/utils.dart';

import 'package:myquizya/src/widgets/form_quice.dart';
import 'package:myquizya/src/page/quices/docente/init_quice_page.dart';

class QuiceIniciarPage extends StatefulWidget {
  QuiceIniciarPage({Key? key, required this.quice}) : super(key: key);

  static final routeName = 'pregunta_ver';

  Quice quice;

  @override
  State<QuiceIniciarPage> createState() => _QuiceIniciarPageState();
}

class _QuiceIniciarPageState extends State<QuiceIniciarPage> {
  late QuicesService quicesService;

  @override
  Widget build(BuildContext context) {
    final scollBarController = ScrollController(initialScrollOffset: 0);

    quicesService = Provider.of<QuicesService>(context);
    quicesService.preguntasSelected = widget.quice.preguntas;
    quicesService.presentacionQuice = new HistorialQuices(quiz: widget.quice);
    quicesService.presentacionQuice.codigo = getRandomString(6);

    return Scaffold(
      appBar: AppBar(
        title: Text('Inciar quiz'),
        backgroundColor: miTema.appBarTheme.backgroundColor,
        actions: [
          // TextButton(onPressed: null, child: Text('50')),
          TextButton(
              onPressed: null,
              child: Text(
                widget.quice.displayTotalPuntos(),
                style: TextStyle(color: Colors.white),
              )),
        ],
      ),
      body: Container(
        child: Scrollbar(
          thickness: 10,
          isAlwaysShown: true,
          controller: scollBarController,
          child: ListView(
            controller: scollBarController,
            shrinkWrap: true,
            children: [
              Column(
                children: <Widget>[
                  _formInicioQuiz(),
                  Text(
                    'Información del quiz',
                    style: TextStyle(color: miTema.primaryColor, fontSize: 25),
                  ),
                  FormQuice(quice: widget.quice, view: true),
                ],
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, InitQuicePage.routeName);
        },
        child: Icon(Icons.play_arrow),
      ),
    );
  }

  Widget _formInicioQuiz() {
    return Container(
      padding: EdgeInsets.only(right: 20.0, top: 8.0, left: 16.0, bottom: 24.0),
      child: Column(
        children: <Widget>[
          SizedBox(height: 5),
          TextFormField(
            initialValue: '',
            enabled: true,
            decoration: InputDecoration(labelText: 'Grupo'),
            onChanged: (value) =>
                quicesService.presentacionQuice.grupo = value.toString(),
          ),
        ],
      ),
    );
  }
}
