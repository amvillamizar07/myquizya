import 'package:flutter/material.dart';
import 'package:myquizya/src/page/quices/alumno/detalle_quiz_alum_page.dart';
import 'package:myquizya/src/page/quices/docente/historial_detalle_page.dart';

class HistoriaQuicesPage extends StatefulWidget {
  const HistoriaQuicesPage({Key? key}) : super(key: key);

  static const routeName = 'historial_doc_page';

  @override
  State<HistoriaQuicesPage> createState() => _HistoriaQuicesPageState();
}

class _HistoriaQuicesPageState extends State<HistoriaQuicesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Historial de quices'),
      ),
      body: _page(),
    );
  }

  Widget _page() {
    return SingleChildScrollView(
      child: Column(
        children: [
          _camposFiltrado(),
          _listaParticipantes(),
        ],
      ),
    );
  }

  Widget _camposFiltrado() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: Column(
        children: [
          _nombreQuice(),
          _camposBusqueda(),
        ],
      ),
    );
  }

  Widget _nombreQuice() {
    return TextFormField(
      initialValue: '',
      enabled: true,
      decoration: InputDecoration(
        labelText: 'Nombre quiz',
        icon: Icon(Icons.search),
      ),
    );
  }

  Widget _nombreAlumno() {
    return TextFormField(
      initialValue: '',
      enabled: true,
      decoration: InputDecoration(
        labelText: 'Nombre Alumno',
        icon: Icon(Icons.person),
      ),
    );
  }

  Widget _nombreGrupo() {
    return TextFormField(
      initialValue: '',
      enabled: true,
      decoration: InputDecoration(
        labelText: 'Grupo',
        icon: Icon(Icons.group_rounded),
      ),
    );
  }

  Widget _diaQuiz() {
    return TextFormField(
      initialValue: '',
      keyboardType: TextInputType.datetime,
      enabled: true,
      decoration: InputDecoration(
        labelText: 'Día',
        icon: Icon(Icons.date_range),
      ),
    );
  }

  Widget _mesQuiz() {
    return TextFormField(
      initialValue: '',
      keyboardType: TextInputType.datetime,
      enabled: true,
      decoration: InputDecoration(
        labelText: 'Mes',
        icon: Icon(Icons.date_range),
      ),
    );
  }

  Widget _anioQuiz() {
    return TextFormField(
      initialValue: '',
      enabled: true,
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(
        labelText: 'Año',
        icon: Icon(Icons.date_range),
      ),
    );
  }

  Widget _camposBusqueda() {
    return Container(
      child: Column(
        children: [
          Table(
            children: [
              TableRow(
                children: [
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: _diaQuiz()),
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: _mesQuiz()),
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: _anioQuiz()),
                ],
              ),
            ],
          ),
          Table(
            children: [
              TableRow(
                children: [
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: _nombreAlumno()),
                  Container(
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      child: _nombreGrupo()),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _listaParticipantes() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: 20.0),
      child: DataTable(
        columns: [
          DataColumn(
            label: Text('Código'),
          ),
          DataColumn(
            label: Text('Fecha'),
          ),
          DataColumn(
            label: Text('Acción'),
          ),
        ],
        rows: [
          _itemLista('HN87IO', '15/03/2021'),
          _itemLista('AAAUI8', '07/03/2021'),
          _itemLista('89HYT0', '18/05/2021'),
          _itemLista('QAO809', '07/05/2021'),
          _itemLista('9JJ8CA', '07/08/2021'),
          _itemLista('6C6HHA', '07/09/2021'),
          _itemLista('1AX7XV', '28/10/2021'),
          _itemLista('1LO9NZ', '10/11/2021'),
          _itemLista('TC621H', '15/11/2021'),
        ],
      ),
    );
  }

  DataRow _itemLista(String codigo, String fecha) {
    return DataRow(
      cells: <DataCell>[
        DataCell(Text(codigo)),
        DataCell(Text(fecha)),
        DataCell(IconButton(
            onPressed: () {
              Navigator.pushNamed(context, HistorialDetallePage.routeName);
            },
            icon: Icon(Icons.remove_red_eye_outlined,
                color: Colors.greenAccent))),
      ],
    );
  }
}
