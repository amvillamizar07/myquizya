import 'package:flutter/material.dart';
import 'dart:math';

enum ListaAcciones { show, edit, initQuiz }

bool isNumeric(String s) {
  if (s.isEmpty) return false;

  // Prueba si se puede pasar a numero
  final n = num.tryParse(s);

  return (n == null) ? false : true;
}

void mostrarAlerta(
    {required BuildContext context,
    String title = '',
    required String mensaje}) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(mensaje),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                },
                child: Text('Entendido'))
          ],
        );
      });
}

Map<String, Map<String, dynamic>> listaTiposPreguntas() {
  return {
    'multiple_respuesta': {
      'text': 'Casillas de verificación',
      'icon': Icons.push_pin_outlined
    },
    'bool': {'text': 'Verdadero o Falso', 'icon': Icons.push_pin_outlined},
    'unica_opcion': {
      'text': 'Opción múltiple',
      'icon': Icons.push_pin_outlined
    },
  };
}

Map<String, Map<String, dynamic>> listaMaterias() {
  return {
    'programacion_i': {
      'text': 'Programacion I',
      'icon': Icons.keyboard_alt_outlined
    },
    'programacion_ii': {
      'text': 'Programacion II',
      'icon': Icons.keyboard_alt_outlined
    },
    'programacion_iii': {
      'text': 'Programacion III',
      'icon': Icons.keyboard_alt_outlined
    },
  };
}

const _chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

String formaterTiempo(String typeDate, int segundos) {
  String dosValores(int valor) {
    return valor >= 10 ? "$valor" : "0$valor";
  }

  Duration duration = Duration(seconds: segundos);
  String response = '';
  switch (typeDate) {
    case 'minuto':
      String minutos = dosValores(duration.inMinutes.remainder(60));
      response = '$minutos';
      break;
    case 'segundos':
      String seconds = dosValores(duration.inSeconds.remainder(60));
      response = '$seconds';
      break;
  }
  return response;
}
