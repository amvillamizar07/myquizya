import 'package:flutter/material.dart';

import 'package:myquizya/src/page/perfil_page.dart';
import 'package:myquizya/src/page/preguntas/pregunta_crear_page.dart';
import 'package:myquizya/src/page/quices/alumno/detalle_quiz_alum_page.dart';
import 'package:myquizya/src/page/historial_alum_page.dart';
import 'package:myquizya/src/page/quices/alumno/init_quice_alum_page.dart';
import 'package:myquizya/src/page/home_alumno_page.dart';
import 'package:myquizya/src/page/quices/alumno/tabs_quiz_page.dart';
import 'package:myquizya/src/page/quices/docente/historial_detalle_page.dart';
import 'package:myquizya/src/page/quices/docente/init_quice_page.dart';
import 'package:myquizya/src/page/login_page.dart';
import 'package:myquizya/src/page/registro_page.dart';
import 'package:myquizya/src/page/tabs_page.dart';

import 'package:provider/provider.dart';

import 'package:myquizya/src/services/quices_service.dart';
import 'package:myquizya/src/services/usuario_service.dart';
import 'package:myquizya/src/services/pregunstas_service.dart';

import 'package:myquizya/src/theme/thema.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => PreguntasService()),
        ChangeNotifierProvider(create: (_) => QuicesService()),
        ChangeNotifierProvider(create: (_) => UsuarioService()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'MyQuizYa',
        theme: miTema,
        initialRoute: LoginPage.routeName,
        // _prefs.token != '' ? HomePage.routeName : LoginPage.routeName,
        routes: {
          TabsPage.routeName: (BuildContext context) => TabsPage(),
          LoginPage.routeName: (BuildContext context) => LoginPage(),
          RegistroPage.routeName: (BuildContext context) => RegistroPage(),
          InitQuicePage.routeName: (BuildContext context) => InitQuicePage(),
          TabsQuizPage.routeName: (BuildContext context) => TabsQuizPage(),
          HomeAlumnoPage.routeName: (BuildContext context) => HomeAlumnoPage(),
          HistorialAlu.routeName: (BuildContext context) => HistorialAlu(),
          PerfilPage.routeName: (BuildContext context) => PerfilPage(),
          HistorialDetallePage.routeName: (BuildContext context) =>
              HistorialDetallePage(),
          // PreguntaCrearPage.routeName: (BuildContext context) =>
          //     PreguntaCrearPage(main: true),
          DetalleQuizAlum.routeName: (BuildContext context) =>
              DetalleQuizAlum(),
          InitQuiceAlumPage.routeName: (BuildContext context) =>
              InitQuiceAlumPage(),
        },
      ),
    );
  }
}
